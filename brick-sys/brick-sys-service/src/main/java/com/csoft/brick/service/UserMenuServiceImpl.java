package com.csoft.brick.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.csoft.brick.common.utils.id.IDGeneratorUtil;
import com.csoft.brick.mapper.UserMenuMapper;
import com.csoft.brick.model.UserMenu;
import com.csoft.brick.provider.IUserMenuService;
import com.csoft.brick.support.BaseServiceImpl;

@Component
@Service("userMenuService")
public class UserMenuServiceImpl extends BaseServiceImpl<UserMenuMapper, UserMenu> implements IUserMenuService {
	@Autowired
	private UserMenuMapper userMenuMapper;

	@Override
	public List<UserMenu> listUserMenus(String userId) {
		EntityWrapper<UserMenu> ew  = new EntityWrapper<UserMenu>();
		ew.eq("user_id", userId);
		List<UserMenu> list = super.selectList(ew);
		return list;
	}

	@Override
	public void addOrDelUserMenus(String userMenus, String userId) {
		userMenuMapper.deleteUserMenus(userId);
		JSONArray userMenuArray = JSONArray.parseArray(userMenus);
		if(userMenuArray.size()>0){
			List<UserMenu> userMenuList = new ArrayList<UserMenu>();
			for(int i=0;i<userMenuArray.size();i++){
				UserMenu userMenu = new UserMenu();
				userMenu.setId(IDGeneratorUtil.getUuidAsId());
				userMenu.setMenuId(userMenuArray.get(i).toString());
				userMenu.setUserId(userId);
				userMenuList.add(userMenu);
			}
			super.insertBatch(userMenuList);
		}
	}
}
