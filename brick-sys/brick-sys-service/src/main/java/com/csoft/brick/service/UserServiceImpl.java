package com.csoft.brick.service;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.csoft.brick.common.utils.date.DateUtil;
import com.csoft.brick.common.utils.id.IDGeneratorUtil;
import com.csoft.brick.mapper.UserMapper;
import com.csoft.brick.mapper.UserMenuMapper;
import com.csoft.brick.mapper.UserRoleMapper;
import com.csoft.brick.model.User;
import com.csoft.brick.provider.IUserService;
import com.csoft.brick.support.BaseServiceImpl;

@Component
@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements IUserService {
	
	@Autowired
	private UserMenuMapper userMenuMapper;
	
	@Autowired
	private UserRoleMapper userRoleMapper;

	@Override
	public Map<String, Object> listUser(String userName,String page,String rows) {
		EntityWrapper<User> ew  = new EntityWrapper<User>();
		ew.like("user_name", userName);
		Page<User> searchPage = new Page<User>(Integer.parseInt(page), Integer.parseInt(rows));
		Page<User> list = super.selectPage(searchPage,ew);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("rows", list.getRecords());
		map.put("total", list.getTotal());
		return map;
	}

	@Override
	public void addUser(User user) {
		user.setId(IDGeneratorUtil.getCurrentDateAsId());
		user.setCreateTime(DateUtil.getLongDateTime());
		super.insert(user);
	}

	@Override
	public void delUser(String id) {
		// 删除用户功能菜单关联表
		userMenuMapper.deleteUserMenus(id);
		
		// 删除用户角色关联表
		userRoleMapper.deleteUserRoles(id);
		
		// 删除用户信息
		super.deleteById(id);
	}

	@Override
	public User getUser(String id) {
		return super.selectById(id);
	}

	@Override
	public void editUser(User user) {
		super.updateById(user);
	}

	@Override
	public User userLogin(String userAccount, String userPassword) {
		EntityWrapper<User> ew  = new EntityWrapper<User>();
		ew.eq("user_account", userAccount);
//		ew.eq("user_password", userPassword);
		return super.selectOne(ew);
	}

}
