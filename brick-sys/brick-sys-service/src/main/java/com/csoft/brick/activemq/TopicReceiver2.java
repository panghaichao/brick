package com.csoft.brick.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.stereotype.Component;

/**
 * 主题（Topic）消费者 2
 * @author Hejeo
 *
 */
@Component
public class TopicReceiver2 implements MessageListener{

	@Override
	public void onMessage(Message message) {
		try {
            System.out.println("TopicReceiver2接收到消息:"+((TextMessage)message).getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }
	}

}
