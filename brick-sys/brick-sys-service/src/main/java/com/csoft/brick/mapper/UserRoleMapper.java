package com.csoft.brick.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.csoft.brick.model.UserRole;

public interface UserRoleMapper extends BaseMapper<UserRole> {
	/**
	 * 根据用户id删除用户的所有角色
	 * @param userId 用户id
	 */
	public void deleteUserRoles(String userId);
}
