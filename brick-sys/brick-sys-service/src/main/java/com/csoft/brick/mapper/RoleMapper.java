package com.csoft.brick.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.csoft.brick.model.Role;
public interface RoleMapper extends BaseMapper<Role>{

}
