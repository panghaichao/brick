package com.csoft.brick.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.csoft.brick.model.UserMenu;

public interface UserMenuMapper extends BaseMapper<UserMenu> {
	/**
	 * 根据用户id删除用户的所有权限
	 * @param userId 用户id
	 */
	public void deleteUserMenus(String userId);
}
