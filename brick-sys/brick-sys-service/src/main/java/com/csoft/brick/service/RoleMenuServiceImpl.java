package com.csoft.brick.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.csoft.brick.common.utils.id.IDGeneratorUtil;
import com.csoft.brick.mapper.RoleMenuMapper;
import com.csoft.brick.model.RoleMenu;
import com.csoft.brick.provider.IRoleMenuService;
import com.csoft.brick.support.BaseServiceImpl;

@Component
@Service("roleMenuService")
public class RoleMenuServiceImpl extends BaseServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {
	@Autowired
	private RoleMenuMapper roleMenuMapper;

	@Override
	public List<RoleMenu> listRoleMenus(String roleId) {
		EntityWrapper<RoleMenu> ew  = new EntityWrapper<RoleMenu>();
		ew.eq("role_id", roleId);
		List<RoleMenu> list = super.selectList(ew);
		return list;
	}

	@Override
	public void addOrDelRoleMenus(String roleMenus, String roleId) {
		roleMenuMapper.deleteRoleMenus(roleId);
		JSONArray roleMenuArray = JSONArray.parseArray(roleMenus);
		if(roleMenuArray.size()>0){
			List<RoleMenu> roleMenuList = new ArrayList<RoleMenu>();
			for(int i=0;i<roleMenuArray.size();i++){
				RoleMenu roleMenu = new RoleMenu();
				roleMenu.setId(IDGeneratorUtil.getUuidAsId());
				roleMenu.setMenuId(roleMenuArray.get(i).toString());
				roleMenu.setRoleId(roleId);
				roleMenuList.add(roleMenu);
			}
			super.insertBatch(roleMenuList);
		}
	}

	
	
}
