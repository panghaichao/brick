package com.csoft.brick.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.csoft.brick.common.annotation.SystemServiceLog;
import com.csoft.brick.mapper.RoleMapper;
import com.csoft.brick.mapper.RoleMenuMapper;
import com.csoft.brick.model.Role;
import com.csoft.brick.provider.IRoleService;
import com.csoft.brick.support.BaseServiceImpl;

@Component
@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, Role> implements IRoleService{
	
	@Autowired
	private RoleMenuMapper roleMenuMapper;

	@Override
	public Map<String, Object> listRole(String roleName, String page, String rows) {
		EntityWrapper<Role> ew  = new EntityWrapper<Role>();
		ew.like("role_name", roleName);
		Page<Role> searchPage = new Page<Role>(Integer.parseInt(page), Integer.parseInt(rows));
		Page<Role> list = super.selectPage(searchPage,ew);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("rows", list.getRecords());
		map.put("total", list.getTotal());
		return map;
	}

	@Override
	@SystemServiceLog(description = "添加角色")   
	public void addRole(Role role) {
		super.insert(role);
	}

	@Override
	public void delRole(String id) {
		// 删除角色功能菜单关联表
		roleMenuMapper.deleteRoleMenus(id);
		super.deleteById(id);
	}

	@Override
	public Role getRole(String id) {
		return super.selectById(id);
	}

	@Override
	public void editRole(Role role) {
		super.updateById(role);
	}

	@Override
	public List<Role> getRoleList() {
		EntityWrapper<Role> ew  = new EntityWrapper<Role>();
		return super.selectList(ew);
	}

}
