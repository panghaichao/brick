package com.csoft.brick.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.csoft.brick.model.User;

public interface UserMapper extends BaseMapper<User> {

}
