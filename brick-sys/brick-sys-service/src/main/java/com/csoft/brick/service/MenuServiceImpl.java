package com.csoft.brick.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.csoft.brick.common.utils.id.IDGeneratorUtil;
import com.csoft.brick.mapper.MenuMapper;
import com.csoft.brick.model.Menu;
import com.csoft.brick.model.MenuTree;
import com.csoft.brick.provider.IMenuService;
import com.csoft.brick.support.BaseServiceImpl;

@Component
@Service("menuService")
public class MenuServiceImpl extends BaseServiceImpl<MenuMapper, Menu> implements IMenuService {
	
	@Autowired
	private MenuMapper menuMapper;
	
	@Override
	public List<MenuTree> getMenuListByLevelId(String levelId) {
		EntityWrapper<Menu> ew  = new EntityWrapper<Menu>();
		ew.eq("menu_level", levelId);
		ew.orderBy("menu_sort");
		List<Menu> list = super.selectList(ew);
		List<MenuTree> listMenuTree = new ArrayList<MenuTree>();
		if(list.size()>0){
			for(Menu m : list){
				MenuTree mt = new MenuTree();
				mt.setId(m.getId());
				mt.setPid(m.getpMenuId());
				mt.setText(m.getMenuName());
				mt.setMenuCode(m.getMenuCode());
				mt.setMenuLevel(m.getMenuLevel());
				mt.setMenuType(m.getMenuType());
				mt.setState(m.getMenuState());
				mt.setUrl(m.getMenuUrl());
				mt.setIconCls(m.getMenuIcon());
				mt.setMenuSort(m.getMenuSort());
				listMenuTree.add(mt);
			}
		}
		return listMenuTree;
	}
	
	
	@Override
	public List<MenuTree> getMenuListByPMenuId(String pMenuId) {
		EntityWrapper<Menu> ew  = new EntityWrapper<Menu>();
		ew.eq("p_menu_id", pMenuId);
		ew.orderBy("menu_sort");
		List<Menu> list = super.selectList(ew);
		List<MenuTree> listMenuTree = new ArrayList<MenuTree>();
		if(list.size()>0){
			for(Menu m : list){
				MenuTree mt = new MenuTree();
				mt.setId(m.getId());
				mt.setPid(m.getpMenuId());
				mt.setText(m.getMenuName());
				mt.setMenuCode(m.getMenuCode());
				mt.setMenuLevel(m.getMenuLevel());
				mt.setMenuType(m.getMenuType());
				mt.setState(m.getMenuState());
				mt.setUrl(m.getMenuUrl());
				mt.setIconCls(m.getMenuIcon());
				mt.setMenuSort(m.getMenuSort());
				listMenuTree.add(mt);
			}
		}
		return listMenuTree;
	}
	

	@Override
	public Map<String, Object> listMenu(String menuName) {
		EntityWrapper<Menu> ew  = new EntityWrapper<Menu>();
		ew.like("menu_name", menuName);
		ew.orderBy("menu_sort");
		
		List<Menu> list = super.selectList(ew);
		Map<String,Object> map = new HashMap<String,Object>();
		
		List<Menu> afterlist = new ArrayList<Menu>();
		for(Menu menu :list){
			if(StringUtils.isNotEmpty(menu.getpMenuId())){
//				menu.set_parentId(menu.getpMenuId());
			}
			afterlist.add(menu);
		}
		
		
		map.put("rows", afterlist);
		map.put("total", afterlist.size());
		return map;
	}
	
	@Override
	public Map<String, Object> listMenuNew(String menuName) {
		
		
		/**
		 * 只查询菜单类型为 0 模块 1菜单 的集合    不查按钮
		 */
		EntityWrapper<Menu> ew  = new EntityWrapper<Menu>();
		ew.like("menu_name", menuName);
		ew.notLike("menu_type", "2");
		ew.orderBy("menu_sort");
		List<Menu> list = super.selectList(ew);
		Map<String,Object> map = new HashMap<String,Object>();
		
		
		List<Menu> afterlist = new ArrayList<Menu>();
		for(Menu menu :list){
			if(StringUtils.isNotEmpty(menu.getpMenuId())){
//				menu.set_parentId(menu.getpMenuId());
			}
			
			if("1".equals(menu.getMenuType())){
//				menu.setFunctionList(getFunctionList(menu.getId()));
			}else{
//				menu.setFunctionList(new ArrayList<Menu>());
			}
			afterlist.add(menu);
		}
		
		map.put("rows", afterlist);
		map.put("total", afterlist.size());
		return map;
	}
	
	
	/**
	 * 获取操作按钮集合
	 * @param menuId 菜单id
	 * @return 操作按钮集合
	 */
	private List<Menu> getFunctionList(String menuId){
		EntityWrapper<Menu> ew  = new EntityWrapper<Menu>();
		ew.eq("p_menu_id", menuId);
		ew.orderBy("menu_sort");
		List<Menu> list = super.selectList(ew);
		for(Menu menu :list){
			if(StringUtils.isNotEmpty(menu.getpMenuId())){
//				menu.set_parentId(menu.getpMenuId());
			}
		}
		return list;
	}
	

	@Override
	public void addMenu(Menu menu) {
		menu.setId(IDGeneratorUtil.getCurrentDateAsId());
		super.insert(menu);
	}

	@Override
	public void delMenu(String id) {
		super.deleteById(id);
	}

	@Override
	public Menu getMenu(String id) {
		return super.selectById(id);
	}

	@Override
	public void editMenu(Menu menu) {
		super.updateById(menu);
	}

	@Override
	public List<Menu> getMenuListByUserIdAndPMenuIdAndMenuLevel(String userId, String pMenuId, String menuLevel) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("userId", userId);
		map.put("pMenuId", pMenuId);
		map.put("menuLevel", menuLevel);
		return menuMapper.getMenuListByUserIdAndPMenuIdAndMenuLevel(map);
	}
	
	@Override
	public List<MenuTree> getMenuListByUserIdAndPMenuId(String userId, String pMenuId) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("userId", userId);
		map.put("pMenuId", pMenuId);
		return menuMapper.getMenuListByUserIdAndPMenuId(map);
	}
}
