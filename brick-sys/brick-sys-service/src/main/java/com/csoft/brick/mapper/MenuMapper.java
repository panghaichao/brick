package com.csoft.brick.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.csoft.brick.model.Menu;
import com.csoft.brick.model.MenuTree;

public interface MenuMapper extends BaseMapper<Menu> {
	public List<Menu> getMenuListByUserIdAndPMenuIdAndMenuLevel(Map<String,String> map);
	
	public List<MenuTree> getMenuListByUserIdAndPMenuId(Map<String,String> map);
	
}
