package com.csoft.brick.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.csoft.brick.common.utils.id.IDGeneratorUtil;
import com.csoft.brick.mapper.UserRoleMapper;
import com.csoft.brick.model.UserRole;
import com.csoft.brick.provider.IUserRoleService;
import com.csoft.brick.support.BaseServiceImpl;

@Component
@Service("userRoleService")
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {
	@Autowired
	private UserRoleMapper userRoleMapper;

	@Override
	public List<UserRole> listUserRoles(String userId) {
		EntityWrapper<UserRole> ew  = new EntityWrapper<UserRole>();
		ew.eq("user_id", userId);
		List<UserRole> list = super.selectList(ew);
		return list;
	}

	@Override
	public void addOrDelUserRoles(String userRoles, String userId) {
		userRoleMapper.deleteUserRoles(userId);
		JSONArray userRoleArray = JSONArray.parseArray(userRoles);
		if(userRoleArray.size()>0){
			List<UserRole> userRoleList = new ArrayList<UserRole>();
			for(int i=0;i<userRoleArray.size();i++){
				UserRole userRole = new UserRole();
				userRole.setId(IDGeneratorUtil.getUuidAsId());
				userRole.setRoleId(userRoleArray.get(i).toString());
				userRole.setUserId(userId);
				userRoleList.add(userRole);
			}
			super.insertBatch(userRoleList);
		}
	}
}
