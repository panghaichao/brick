package com.csoft.brick.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.csoft.brick.model.RoleMenu;

public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
	/**
	 * 根据角色id删除角色的所有权限
	 * @param roleId 角色id
	 */
	public void deleteRoleMenus(String roleId);
}
