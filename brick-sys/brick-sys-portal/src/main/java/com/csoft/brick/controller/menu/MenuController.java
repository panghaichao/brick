package com.csoft.brick.controller.menu;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.model.Menu;
import com.csoft.brick.model.MenuTree;
import com.csoft.brick.provider.IMenuService;

/**
 * 功能菜单 管理
 * @author Hejeo
 *
 */
@Controller
@RequestMapping("/menuController")
public class MenuController extends BaseController{
	
	@Autowired 
	private IMenuService menuService;
	
	/**
	 * 跳转到功能菜单列表界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toMenuListPage")
	public String toMenuListPage(HttpServletRequest request,Model model){
		return "menu/list_menu";
	}
	
	/**
	 * 获取功能菜单列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/getMenuListByLevelId")
	@SystemControllerLog(description = "获取功能菜单数据")
	@ResponseBody
	public List<MenuTree> getMenuListByLevelId(HttpServletRequest request,Model model){
		String levelId = request.getParameter("levelId");
		return menuService.getMenuListByLevelId(levelId);
	}
	
	
	
	/**
	 * 获取功能菜单列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/getMenuListByPMenuId")
	@SystemControllerLog(description = "获取功能菜单数据")
	@ResponseBody
	public List<MenuTree> getMenuListByPMenuId(HttpServletRequest request,Model model){
		String pMenuId = request.getParameter("pMenuId");
		return menuService.getMenuListByPMenuId(pMenuId);
	}
	
	
	/**
	 * 跳转到添加功能菜单界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAddMenuPage")
	public String toAddMenuPage(HttpServletRequest request,Model model){
		return "menu/add_menu";
	}
	
	
	/**
	 * 添加功能菜单
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addMenu")
	@SystemControllerLog(description = "添加功能菜单")
	@ResponseBody
	public AjaxResult addMenu(Menu menu){
		AjaxResult result = new AjaxResult(true,"添加成功",null);
		try {
			menuService.addMenu(menu);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("添加出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	/**
	 * 删除功能菜单
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/delMenu")
	@SystemControllerLog(description = "删除功能菜单")
	@ResponseBody
	public AjaxResult delMenu(String id){
		AjaxResult result = new AjaxResult(true,"删除成功",null);
		try {
			menuService.delMenu(id);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("删除出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	/**
	 * 跳转到修改功能菜单界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toEditMenuPage")
	public String toEditMenuPage(HttpServletRequest request,Model model){
		model.addAttribute("Menu", menuService.getMenu(request.getParameter("id")));
		return "menu/edit_menu";
	}
	
	/**
	 * 修改功能菜单
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/editMenu")
	@SystemControllerLog(description = "修改功能菜单")
	@ResponseBody
	public AjaxResult editMenu(Menu menu){
		AjaxResult result = new AjaxResult(true,"修改成功",null);
		try {
			menuService.editMenu(menu);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("修改出现异常，请稍后再试！");
		}
		return result;
	}
}
