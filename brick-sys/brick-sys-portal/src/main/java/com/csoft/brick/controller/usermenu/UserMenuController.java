package com.csoft.brick.controller.usermenu;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.common.redis.RedisKey;
import com.csoft.brick.model.User;
import com.csoft.brick.provider.IMenuService;
import com.csoft.brick.provider.IUserMenuService;

@Controller
@RequestMapping("/userMenuController")
public class UserMenuController extends BaseController{
	
	@Autowired 
	private IUserMenuService userMenuService;
	
	@Autowired 
	private IMenuService menuService;
	
	
	/**
	 * 获取功能菜单列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/listUserMenus")
	@SystemControllerLog(description = "获取用户拥有的功能菜单") 
	@ResponseBody
	public Map<String,Object> listUserMenus(HttpServletRequest request,Model model){
		String menuName = request.getParameter("menuName");
		String userId = request.getParameter("userId");
		Map<String,Object> map  = menuService.listMenu(menuName);
		map.put("userMenus", userMenuService.listUserMenus(userId));
		return map;
	}
	
	
	/** 添加或删除用户功能菜单
	 * @param userMenus 用户功能Json数组集合
	 * @param userId 用户id
	 * @return
	 */
	@RequestMapping("/addOrDelUserMenus")
	@SystemControllerLog(description = "用户授权功能菜单") 
	@ResponseBody
	public AjaxResult addOrDelUserMenus(String userMenus,String userId){
		AjaxResult result = new AjaxResult(true,"授权成功",null);
		try {
			userMenuService.addOrDelUserMenus(userMenus, userId);
			User user = (User) getCurrentUser();
			super.redisDAO.hdel(RedisKey.USER_MENUS, user.getId());
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("授权出现异常，请稍后再试！");
		}
		return result;
	}
}
