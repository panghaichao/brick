package com.csoft.brick.common.shiro;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.fastjson.JSON;
import com.csoft.brick.common.redis.RedisDAO;
import com.csoft.brick.common.redis.RedisKey;
import com.csoft.brick.common.redis.ShardedJedisDAO;
import com.csoft.brick.model.Menu;
import com.csoft.brick.model.RoleMenu;
import com.csoft.brick.model.User;
import com.csoft.brick.model.UserMenu;
import com.csoft.brick.model.UserRole;
import com.csoft.brick.provider.IMenuService;
import com.csoft.brick.provider.IRoleMenuService;
import com.csoft.brick.provider.IUserMenuService;
import com.csoft.brick.provider.IUserRoleService;
import com.csoft.brick.provider.IUserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ShiroDbRealm extends AuthorizingRealm {
	@Autowired
	private IUserService userService;
	
	@Autowired 
	private IUserMenuService userMenuService;
	
	@Autowired 
	private IUserRoleService userRoleService;
	
	@Autowired 
	private IRoleMenuService roleMenuService;
	
	@Autowired 
	private IMenuService menuService;
	
	/**
	 * 单个操作redis
	 */
	@Autowired
	protected RedisDAO redisDAO;
	
	/**
	 * 批量操作redis
	 */
	@Autowired
	protected ShardedJedisDAO shardedJedisDAO;


	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		User user = new User();
		user = userService.userLogin(token.getUsername(), String.valueOf(token.getPassword()));
		// 认证缓存信息
		if(user==null){
			return null;
		}
		return new SimpleAuthenticationInfo(user, user.getUserPassword(), getName());
	}

	/**
	 * 
	 * Shiro权限认证
	 * 
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		User user = (User) principals.getPrimaryPrincipal();
		
		
		// 用户拥有的权限集合
		Set<String> urlSet = new HashSet<String>();
		
		/**
		 * 1、先从Redis缓存中获取用户功能菜单集合
		 * 2、如果Redis缓存中不存在则查询数据库并存入Redis中
		 * 3、如果Redis缓存中存在则直接获取并赋给urlSet中
		 */
		
		Map<String, String> userMenusMap = redisDAO.hgetAll(RedisKey.USER_MENUS);
		if(userMenusMap!=null && userMenusMap.size()>0){
			String currentUserMenus = userMenusMap.get(user.getId());
			if(StringUtils.isNotEmpty(currentUserMenus)){
				List<Menu> allMenuList = JSON.parseArray(currentUserMenus, Menu.class);
				for (Menu menu : allMenuList) {
					urlSet.add(menu.getMenuCode());
				}
			}
		}else{
			List<UserMenu> userMenuList = userMenuService.listUserMenus(user.getId());	// 获取用户拥有的功能菜单集合
			List<UserRole> userRoleList = userRoleService.listUserRoles(user.getId());	// 获取用户拥有的角色集合
			List<RoleMenu> allRoleMenuList = new ArrayList<RoleMenu>();	// 所有角色功能菜单集合
			if(userRoleList.size()>0){
				for(UserRole userRole : userRoleList){
					allRoleMenuList.addAll(roleMenuService.listRoleMenus(userRole.getRoleId()));
				}
			}
			
			List<String> menuIdList = new ArrayList<String>();	//	定义出功能菜单Id集合
			
			/*循环用户对应的功能菜单集合  把功能菜单 放入 menuIdList 集合中*/
			if(userMenuList.size()>0){
				for(UserMenu userMenu : userMenuList){
					if(!menuIdList.contains(userMenu.getMenuId())){
						menuIdList.add(userMenu.getMenuId());
					}
				}
			}
			
			/*循环角色对应的功能菜单集合  把功能菜单 放入 menuIdList 集合中*/
			if(allRoleMenuList.size()>0){
				for(RoleMenu roleMenu : allRoleMenuList){
					if(!menuIdList.contains(roleMenu.getMenuId())){
						menuIdList.add(roleMenu.getMenuId());
					}
				}
			}
			
			List<Menu> allMenuList = new ArrayList<Menu>();	// 真正功能菜单集合
			if(menuIdList.size()>0){
				for(String menuId : menuIdList){
					allMenuList.add(menuService.getMenu(menuId));
				}
			}
			
			for (Menu menu : allMenuList) {
				urlSet.add(menu.getMenuCode());
			}
			
			
			// 写入Redis缓存中
			Map<String,String> map = new HashMap<String,String>();
			map.put(user.getId(), JSON.toJSONString(allMenuList));
			redisDAO.hmset(RedisKey.USER_MENUS, map);
		}
		
		
		
		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		info.addStringPermissions(urlSet);
		return info;
	}

}
