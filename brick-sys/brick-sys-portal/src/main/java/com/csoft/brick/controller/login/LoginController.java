package com.csoft.brick.controller.login;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.fastjson.JSON;
import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.common.redis.RedisKey;
import com.csoft.brick.model.Menu;
import com.csoft.brick.model.MenuTree;
import com.csoft.brick.model.RoleMenu;
import com.csoft.brick.model.User;
import com.csoft.brick.model.UserMenu;
import com.csoft.brick.model.UserRole;
import com.csoft.brick.provider.IMenuService;
import com.csoft.brick.provider.IRoleMenuService;
import com.csoft.brick.provider.IUserMenuService;
import com.csoft.brick.provider.IUserRoleService;

@Controller
//@RequestMapping("/loginController")
public class LoginController extends BaseController{
	
//	@Autowired 
//	private IUserService userService;
	
	@Autowired 
	private IUserMenuService userMenuService;
	
	@Autowired 
	private IUserRoleService userRoleService;
	
	@Autowired 
	private IRoleMenuService roleMenuService;
	
	@Autowired 
	private IMenuService menuService;
	
	/**
	 * 跳转到登录界面
	 * @param request
	 * @param model
	 * @return login.html
	 */
	@RequestMapping("/toLogin")
	public String toLogin(HttpServletRequest request,Model model){
		return "login";
	}
	

	/**
	 * 跳转到主页main界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toMain")
	public String toMain(HttpServletRequest request,Model model){
		/**
		 * 1、先从Redis缓存中获取用户功能菜单集合
		 * 2、如果Redis缓存中不存在则查询数据库并存入Redis中
		 * 3、如果Redis缓存中存在则直接获取并赋给urlSet中
		 */
		
		
//		User user=(User) SecurityUtils.getSubject().getPrincipal();
//
//		List<Menu> allMenuList = new ArrayList<Menu>();	// 真正功能菜单集合
//		
//		Map<String, String> userMenusMap = super.redisDAO.hgetAll(RedisKey.USER_MENUS);
//		if(userMenusMap!=null && userMenusMap.size()>0){
//			String currentUserMenus = userMenusMap.get(user.getId());
//			if(StringUtils.isNotEmpty(currentUserMenus)){
//				allMenuList = JSON.parseArray(currentUserMenus, Menu.class);
//			}
//		}else{
//			List<UserMenu> userMenuList = userMenuService.listUserMenus(user.getId());	// 获取用户拥有的功能菜单集合
//			List<UserRole> userRoleList = userRoleService.listUserRoles(user.getId());	// 获取用户拥有的角色集合
//			List<RoleMenu> allRoleMenuList = new ArrayList<RoleMenu>();	// 所有角色功能菜单集合
//			if(userRoleList.size()>0){
//				for(UserRole userRole : userRoleList){
//					allRoleMenuList.addAll(roleMenuService.listRoleMenus(userRole.getRoleId()));
//				}
//			}
//			
//			List<String> menuIdList = new ArrayList<String>();	//	定义出功能菜单Id集合
//			
//			/*循环用户对应的功能菜单集合  把功能菜单 放入 menuIdList 集合中*/
//			if(userMenuList.size()>0){
//				for(UserMenu userMenu : userMenuList){
//					if(!menuIdList.contains(userMenu.getMenuId())){
//						menuIdList.add(userMenu.getMenuId());
//					}
//				}
//			}
//			
//			/*循环角色对应的功能菜单集合  把功能菜单 放入 menuIdList 集合中*/
//			if(allRoleMenuList.size()>0){
//				for(RoleMenu roleMenu : allRoleMenuList){
//					if(!menuIdList.contains(roleMenu.getMenuId())){
//						menuIdList.add(roleMenu.getMenuId());
//					}
//				}
//			}
//			
//			
//			if(menuIdList.size()>0){
//				for(String menuId : menuIdList){
//					allMenuList.add(menuService.getMenu(menuId));
//				}
//			}
//			
//			// 写入Redis缓存中
//			Map<String,String> map = new HashMap<String,String>();
//			map.put(user.getId(), JSON.toJSONString(allMenuList));
//			super.redisDAO.hmset(RedisKey.USER_MENUS, map);
//		}
//		
//		
//		Comparator<Menu> comparator = new Comparator<Menu>() {
//			public int compare(Menu s1, Menu s2) {
//				return s1.getMenuSort() - s2.getMenuSort();
//			}
//		};
//		
//		Collections.sort(allMenuList,comparator);
//		model.addAttribute("allMenuList", allMenuList);
		
		return "main";
	}
	
	
	@RequestMapping("/toInfo")
	public String toInfo(HttpServletRequest request,Model model){
		return "info";
	}
	
	
	/**
	 * 登录
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/userLogin")
	@SystemControllerLog(description = "用户登录") 
	@ResponseBody
	public AjaxResult userLogin(HttpServletRequest request,Model model){
		AjaxResult result = new AjaxResult(true,"登录成功",null);
		
		String userAccount = request.getParameter("userAccount");	// 登录用户帐号
		String userPassword = request.getParameter("userPassword");	// 登录用户帐号密码
		
		Subject shiroUser = SecurityUtils.getSubject();
    	UsernamePasswordToken token = new UsernamePasswordToken(userAccount, userPassword);
    	
		try {
			shiroUser.login(token);
//			taskInit.init(); //开始执行任务
		} catch (UnknownAccountException e) {
			result.setSuccess(false);
			result.setMessage("账号不存在");
		} catch (DisabledAccountException e) {
			result.setSuccess(false);
        	result.setMessage("账号未启用");
        } catch (IncorrectCredentialsException e) {
        	result.setSuccess(false);
        	result.setMessage("密码错误");
        } catch (RuntimeException e) {
        	result.setSuccess(false);
        	result.setMessage("登录出现异常，请稍后再试！");
        	e.printStackTrace();
        }
		return result;
	}
	
	
	/**
	 * 注销
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/userLoginOut")
	@ResponseBody
	@SystemControllerLog(description = "用户注销")
	public AjaxResult userLoginOut(HttpServletRequest request,Model model){
		AjaxResult result = new AjaxResult(true,"注销成功",null);
		try {
			Subject subject = SecurityUtils.getSubject();
	        subject.logout();
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("注销出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	/**
	 * 根据用户编号、上级编号、菜单层级获取菜单集合
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/getMenuListByRoleIdAndCodeSetIdAndLevelId")
	@ResponseBody
	@SystemControllerLog(description = "用户注销")
	public List<Menu> getMenuListByRoleIdAndCodeSetIdAndLevelId(HttpServletRequest request,Model model){
		String menuLevel = request.getParameter("menuLevel");	// 菜单层级
		String menuId = request.getParameter("menuId");	// 菜单主键
		User user=(User) SecurityUtils.getSubject().getPrincipal();
		List<Menu> userMenuList = menuService.getMenuListByUserIdAndPMenuIdAndMenuLevel(user.getId(), menuId, menuLevel);
		return userMenuList;
	}
	
	
	/**
	 * 根据用户编号、上级编号获取菜单集合
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/getMenuListByPMenuId")
	@ResponseBody
	@SystemControllerLog(description = "用户注销")
	public List<MenuTree> getMenuListByPMenuId(HttpServletRequest request,Model model){
		String pMenuId = request.getParameter("pMenuId");	// 菜单主键
		User user=(User) SecurityUtils.getSubject().getPrincipal();
		List<MenuTree> userMenuList = menuService.getMenuListByUserIdAndPMenuId(user.getId(), pMenuId);
		return userMenuList;
	}
	
	
}
