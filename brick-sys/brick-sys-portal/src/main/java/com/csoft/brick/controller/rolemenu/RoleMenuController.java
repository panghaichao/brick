package com.csoft.brick.controller.rolemenu;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.common.redis.RedisKey;
import com.csoft.brick.model.User;
import com.csoft.brick.provider.IMenuService;
import com.csoft.brick.provider.IRoleMenuService;

@Controller
@RequestMapping("/roleMenuController")
public class RoleMenuController extends BaseController{

	@Autowired 
	private IRoleMenuService roleMenuService;
	
	
	@Autowired 
	private IMenuService menuService;
	
	
	/**
	 * 获取功能菜单列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/listRoleMenus")
	@ResponseBody
	public Map<String,Object> listRoleMenus(HttpServletRequest request,Model model){
		String menuName = request.getParameter("menuName");
		String roleId = request.getParameter("roleId");
		Map<String,Object> map  = menuService.listMenu(menuName);
		map.put("roleMenus", roleMenuService.listRoleMenus(roleId));
		return map;
	}
	
	/**
	 * 获取功能菜单列表数据  -新方式
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/listRoleMenusNew")
	@ResponseBody
	public Map<String,Object> listRoleMenusNew(HttpServletRequest request,Model model){
		String menuName = request.getParameter("menuName");
		String roleId = request.getParameter("roleId");
		Map<String,Object> map  = menuService.listMenuNew(menuName);
		map.put("roleMenus", roleMenuService.listRoleMenus(roleId));
		return map;
	}
	
	
	/** 添加或删除角色功能菜单
	 * @param roleMenus 角色功能Json数组集合
	 * @param roleId 角色id
	 * @return
	 */
	@RequestMapping("/addOrDelRoleMenus")
	@SystemControllerLog(description = "角色授权功能菜单") 
	@ResponseBody
	public AjaxResult addOrDelRoleMenus(String roleMenus,String roleId){
		AjaxResult result = new AjaxResult(true,"授权成功",null);
		try {
			roleMenuService.addOrDelRoleMenus(roleMenus, roleId);
			User user = (User) super.getCurrentUser();
			super.redisDAO.hdel(RedisKey.USER_MENUS, user.getId());
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("授权出现异常，请稍后再试！");
		}
		return result;
	}
}
