package com.csoft.brick.controller.userrole;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.common.redis.RedisKey;
import com.csoft.brick.model.User;
import com.csoft.brick.model.UserRole;
import com.csoft.brick.provider.IUserRoleService;

@Controller
@RequestMapping("/userRoleController")
public class UserRoleController extends BaseController{
	
	@Autowired 
	private IUserRoleService userRoleService;
	
	/**
	 * 获取用户角色列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/listUserRoles")
	@SystemControllerLog(description = "获取用户角色列表数据") 
	@ResponseBody
	public List<UserRole> listUserRoles(HttpServletRequest request,Model model){
		String userId = request.getParameter("userId");
		return userRoleService.listUserRoles(userId);
	}
	
	
	/** 添加或删除用户角色
	 * @param userRoles 用户功能Json数组集合
	 * @param userId 用户id
	 * @return
	 */
	@RequestMapping("/addOrDelUserRoles")
	@SystemControllerLog(description = "用户授权角色") 
	@ResponseBody
	public AjaxResult addOrDelUserRoles(String userRoles,String userId){
		AjaxResult result = new AjaxResult(true,"授权成功",null);
		try {
			userRoleService.addOrDelUserRoles(userRoles, userId);
			User user = (User) super.getCurrentUser();
			super.redisDAO.hdel(RedisKey.USER_MENUS, user.getId());
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("授权出现异常，请稍后再试！");
		}
		return result;
	}
}
