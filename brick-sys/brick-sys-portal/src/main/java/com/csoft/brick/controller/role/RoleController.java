package com.csoft.brick.controller.role;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.common.utils.date.DateUtil;
import com.csoft.brick.common.utils.id.IDGeneratorUtil;
import com.csoft.brick.model.Role;
import com.csoft.brick.provider.IRoleService;

@Controller
@RequestMapping("/roleController")
public class RoleController extends BaseController{

	@Autowired 
	private IRoleService roleService;
	
	/**
	 * 跳转到角色列表界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toRoleListPage")
	public String toRoleListPage(HttpServletRequest request,Model model){
		return "role/list_role";
	}
	
	/**
	 * 获取角色列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/listRole")
	@SystemControllerLog(description = "获取角色列表数据")
	@ResponseBody
	public Map<String,Object> listRole(HttpServletRequest request,Model model){
		String roleName = request.getParameter("roleName");
		String page = request.getParameter("page");
		String rows = request.getParameter("rows");
		Map<String,Object> map = roleService.listRole(roleName, page, rows);
		return map;
	}
	
	
	/**
	 * 跳转到添加角色面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAddRolePage")
	public String toAddRolePage(HttpServletRequest request,Model model){
		return "role/add_role";
	}
	
	
	/**
	 * 添加角色
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addRole")
	@ResponseBody
	@SystemControllerLog(description = "添加角色") 
	public AjaxResult addRole(Role role){
		AjaxResult result = new AjaxResult(true,"添加成功",null);
		try {
			role.setId(IDGeneratorUtil.getCurrentDateAsId());
			role.setCreateTime(DateUtil.getLongDateTime());
			roleService.addRole(role);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("添加出现异常，请稍后再试！");
		}
		return result;
	}
	
	/**
	 * 删除角色
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/delRole")
	@SystemControllerLog(description = "删除角色") 
	@ResponseBody
	public AjaxResult delRole(String id){
		AjaxResult result = new AjaxResult(true,"删除成功",null);
		try {
			roleService.delRole(id);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("删除出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	/**
	 * 跳转到修改角色面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toEditRolePage")
	public String toEditRolePage(HttpServletRequest request,Model model){
		model.addAttribute("Role", roleService.getRole(request.getParameter("id")));
		return "role/edit_role";
	}
	
	/**
	 * 修改角色
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/editRole")
	@SystemControllerLog(description = "修改角色") 
	@ResponseBody
	public AjaxResult editRole(Role role){
		AjaxResult result = new AjaxResult(true,"修改成功",null);
		try {
			roleService.editRole(role);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("修改出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	
	/**
	 * 跳转到用户授权界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAuthorizePage")
	public String toAuthorizePage(HttpServletRequest request,Model model){
		model.addAttribute("roleId", request.getParameter("roleId"));
		return "role/authorize_role";
	}
	
	
	/**
	 * 跳转到角色列表界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAuthorizePageNew")
	public String toAuthorizePageNew(HttpServletRequest request,Model model){
		model.addAttribute("roleId", request.getParameter("roleId"));
		return "role/authorize_role_new";
	}
	
	
	/**
	 * 获取角色列表
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/getRoleList")
	@ResponseBody
	public List<Role> getRoleList(HttpServletRequest request,Model model){
		return roleService.getRoleList();
	}
	
	
}
