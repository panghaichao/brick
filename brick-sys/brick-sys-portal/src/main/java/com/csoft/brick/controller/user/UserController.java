package com.csoft.brick.controller.user;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.alibaba.fastjson.JSONObject;
import com.csoft.brick.common.annotation.SystemControllerLog;
import com.csoft.brick.common.controller.AjaxResult;
import com.csoft.brick.common.controller.BaseController;
import com.csoft.brick.common.qiniu.AccountMgr;
import com.csoft.brick.common.qiniu.QiniuSimpleUpload;
import com.csoft.brick.common.utils.date.DateUtil;
import com.csoft.brick.common.websocket.WebsocketUtils;
import com.csoft.brick.model.User;
import com.csoft.brick.provider.IUserService;
import sun.misc.BASE64Decoder;

@Controller
@RequestMapping("/userController")
public class UserController extends BaseController{

	@Autowired 
	private IUserService userService;
	
	/**
	 * 跳转到用户列表界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toUserListPage")
	public String toUserListPage(HttpServletRequest request,Model model){
		return "user/list_user";
	}
	
	
	/**
	 * 获取用户列表数据
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/listUser")
	@SystemControllerLog(description = "获取用户列表数据") 
	@ResponseBody
	public Map<String,Object> listUser(HttpServletRequest request,Model model){
		String userName = request.getParameter("userName");
		String page = request.getParameter("page");
		String rows = request.getParameter("rows");
		Map<String,Object> map = userService.listUser(userName, page, rows);
		
		
		User user=(User) super.getCurrentUser();
		JSONObject json = new JSONObject();
		json.put("type", "120");
		json.put("notice_title", 0);
		json.put("notice_content", "哈哈哈哈测试");
		WebsocketUtils.sendMessage(user.getId(),json.toJSONString());
		return map;
	}
	
	
	/**
	 * 跳转到添加用户界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAddUserPage")
	public String toAddUserPage(HttpServletRequest request,Model model){
		return "user/add_user";
	}
	
	
	/**
	 * 添加用户
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addUser")
	@SystemControllerLog(description = "添加用户") 
	@ResponseBody
	public AjaxResult addUser(User user){
		AjaxResult result = new AjaxResult(true,"添加成功",null);
		try {
			userService.addUser(user);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("添加出现异常，请稍后再试！");
		}
		return result;
	}
	
	/**
	 * 删除用户
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/delUser")
	@SystemControllerLog(description = "删除用户") 
	@ResponseBody
	public AjaxResult delUser(String id){
		AjaxResult result = new AjaxResult(true,"删除成功",null);
		try {
			userService.delUser(id);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("删除出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	/**
	 * 跳转到修改用户界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toEditUserPage")
	public String toEditUserPage(HttpServletRequest request,Model model){
		model.addAttribute("User", userService.getUser(request.getParameter("id")));
		return "user/edit_user";
	}
	
	
	/**
	 * 跳转到修改用户资料界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toEditUserInformationPage")
	public String toEditUserInformationPage(HttpServletRequest request,Model model){
		model.addAttribute("User", userService.getUser(request.getParameter("id")));
		return "user/edit_user_information";
	}
	
	
	/**
	 * 修改用户
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/editUser")
	@SystemControllerLog(description = "修改用户") 
	@ResponseBody
	public AjaxResult editUser(User user){
		AjaxResult result = new AjaxResult(true,"修改成功",null);
		try {
			userService.editUser(user);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("修改出现异常，请稍后再试！");
		}
		return result;
	}
	
	
	
	/**
	 * 跳转到用户授权界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAuthorizePage")
	public String toAuthorizePage(HttpServletRequest request,Model model){
		model.addAttribute("userId", request.getParameter("userId"));
		return "user/authorize_user";
	}
	
	
	
	
	/**
	 * 跳转到用户角色授权界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAuthorizeRolePage")
	public String toAuthorizeRolePage(HttpServletRequest request,Model model){
		model.addAttribute("userId", request.getParameter("userId"));
		return "user/authorize_user_role";
	}
	
	
	
	/**
	 * 跳转到修改用户头像界面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toEditUserImagePage")
	public String toEditUserImagePage(HttpServletRequest request,Model model){
		model.addAttribute("User", userService.getUser(request.getParameter("id")));
		return "user/edit_user_image";
	}
	
	
	/**
	 * 修改用户头像
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/editUserImage")
	@SystemControllerLog(description = "修改用户头像") 
	@ResponseBody
	public AjaxResult editUserImage(Model model, HttpServletRequest request,HttpServletResponse response){  
		AjaxResult result = new AjaxResult(true,"用户头像修改成功",null);
		
        String filePath="D:/";  
        String fileName=DateUtil.getShortDateTime()+".png";  
        // 参数序列化  
        String image = request.getParameter("image");   //拿到字符串格式的图片  
        String PicName=fileName;  
  
        // 只允许image  
        String header ="data:image";  
        String[] imageArr=image.split(",");  
        if(imageArr[0].contains(header)){//是img的  
	    	// 去掉头部  
	        image=imageArr[1];  
	        //image = image.substring(header.length());  
	        // 写入磁盘  
	        BASE64Decoder decoder = new BASE64Decoder();  
	        try{  
	            byte[] decodedBytes = decoder.decodeBuffer(image);        //将字符串格式的image转为二进制流（biye[])的decodedBytes  
	            String imgFilePath =filePath+PicName;                        //指定图片要存放的位置  
	            File targetFile = new File(filePath);  
	            if(!targetFile.exists()){    
	                targetFile.mkdirs();    
	            }    
	                  
	            FileOutputStream out = new FileOutputStream(imgFilePath);        //新建一个文件输出器，并为它指定输出位置imgFilePath  
	            out.write(decodedBytes); //利用文件输出器将二进制格式decodedBytes输出  
	            out.close();                        //关闭文件输出器  
	            
	            
	            // 调用七牛上传
	            new QiniuSimpleUpload().upload(imgFilePath, fileName);
	            
	            // 更新用户头像
	            User user=(User) SecurityUtils.getSubject().getPrincipal();
	            user.setUserImage(AccountMgr.URL_KEY+fileName);
	            userService.editUser(user);
	            
	        }catch(Exception e){  
	        	e.printStackTrace();
	        	result.setSuccess(false);
				result.setMessage("修改用户头像出现异常，请稍后再试！");
	        }  
        }  
        return result;  
	}
	
	
}
