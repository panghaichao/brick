$(function(){
	// 初始化Web Uploader
	var uploader = WebUploader.create({
		auto: false,  // 选完文件后，是否自动上传。  
		swf: '${basePath}/app/plugin/webuploader/Uploader.swf',  // swf文件路径  
		server: basePathbasePath+'/upload/webuploader',  // 文件接收服务端。  
		pick: {
            id: '#picker',
            label: '选择文件'
        },
		 // 选择文件的按钮。可选。  // 内部根据当前运行是创建，可能是input元素，也可能是flash. 
		// 只允许选择图片文件。  
		accept: {
			title: 'Images',  
			extensions: 'gif,jpg,jpeg,bmp,png',  
			mimeTypes: 'image/jpg,image/jpeg,image/png'
		},
//		fileNumLimit: 9,      //上传文件个数
//      fileSizeLimit: 200 * 1024 * 1024,    // 200 M     上传文件总大小
//      fileSingleSizeLimit: 2 * 1024 * 1024,    // 2 M    单个文件大小
		method:'POST'
	});
	
	
	// 当有文件添加进来的时候
    uploader.on('fileQueued', function( file ) {
    	$("#thelist").append( 
    			'<div id="' + file.id + '" class="item">' +
    			'<img>' +
    	        '<h4 class="info">' + file.name + '</h4>' +
    	        '<p class="state">等待上传...</p>' +
    	    	'</div>' );
    	
    	var $li = $( '#'+file.id );
    	var $img = $li.find('img');
    	
    	// 创建缩略图
        // 如果为非图片文件，可以不用调用此方法。
        // src 是 base64 格式的图片
        uploader.makeThumb(file, function(error, src) {
            if (error) {
                $img.replaceWith('<span>不能预览</span>');
                return;
            }
            $img.attr('src', src);
        }, 100, 100); // 100 * 100 为缩略图多大小
        
        
    });
    
    
    // 文件上传过程中创建进度条实时显示。
    uploader.on('uploadProgress', function( file, percentage ) {
    	var $li = $( '#'+file.id );
        var $percent = $li.find('.progress .progress-bar');

	    // 避免重复创建
	    if ( !$percent.length ) {
	        $percent = $('<div class="progress progress-striped active">' +
	          '<div class="progress-bar" role="progressbar" style="width: 0%">' +
	          '</div>' +
	        '</div>').appendTo( $li ).find('.progress-bar');
	    }
	
	    $li.find('p.state').text('上传中');
	
	    $percent.text(percentage * 100 + '%' );
    });
    
    
    uploader.on( 'uploadSuccess', function( file ) {
        $( '#'+file.id ).find('p.state').text('已上传');
    });

    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });
	
    
    
    
    $("#ctlBtn").on('click', function() {
    	uploader.upload();
    });
	
	
	
	
});  