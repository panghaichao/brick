var mainsocket = new gcfp.websocket();

gcfp.mainWebSocket = mainsocket;

mainsocket.init(contextPath,currentUserId);


mainsocket.addOnOpenListener("main",function(){
	//alert("连接成功");
	//mainsocket.sendMessage("测试主动发消息")
	
	//心跳检测重置
    heartCheck.reset().start();
})

mainsocket.addOnCloseListener("main",function(){
	$.messager.alert('温馨提示','您已经与服务器断开连接...','error',function(){
		location.href = contextPath+"/toLogin?_t=" + new Date().getTime();
	});
});


mainsocket.addOnMessageListener("deviceImport",function(json){
	//alert("收到消息")
	//console.log(json);
	heartCheck.reset().start();
});


//心跳检测
var heartCheck = {
    timeout: 60000,//60秒
    timeoutObj: null,
    serverTimeoutObj: null,
    reset: function(){
        clearTimeout(this.timeoutObj);
        clearTimeout(this.serverTimeoutObj);
        return this;
    },
    start: function(){
        var self = this;
        this.timeoutObj = setTimeout(function(){
            //这里发送一个心跳，后端收到后，返回一个心跳消息，
            //onmessage拿到返回的心跳就说明连接正常
        	mainsocket.sendMessage("主动发消息保持心跳")
            self.serverTimeoutObj = setTimeout(function(){//如果超过一定时间还没重置，说明后端主动断开了
            	mainsocket.closeWebSocket();//如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
            }, self.timeout)
        }, this.timeout)
    }
}


