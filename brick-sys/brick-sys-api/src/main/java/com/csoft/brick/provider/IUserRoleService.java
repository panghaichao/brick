package com.csoft.brick.provider;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.csoft.brick.model.UserRole;

public interface IUserRoleService extends IService<UserRole> {
	/**
	 * 根据用户id获取用户的角色集合
	 * @param userId 用户id
	 * @return
	 */
	public List<UserRole> listUserRoles(String userId);
	
	/** 添加或删除用户角色
	 * @param userRoles 用户角色Json数组集合
	 * @param userId 用户id
	 */
	public void addOrDelUserRoles(String userRoles,String userId);
}
