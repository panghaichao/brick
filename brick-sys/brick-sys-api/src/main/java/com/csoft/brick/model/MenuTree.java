package com.csoft.brick.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * @author Hejeo
 * 功能菜单表
 */
@TableName(value = "t_sys_menu")
public class MenuTree implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 主键
	 */
	private String id;
	
	/**
	 * 上级菜单编号
	 */
	@TableField(value="p_menu_id")
	private String pid;
	
	/**
	 * 菜单名称
	 */
	@TableField(value="menu_name")
	private String text;
	
	/**
	 * 菜单编码
	 */
	@TableField(value="menu_code")
	private String menuCode;
	
	/**
	 * 菜单层级
	 */
	@TableField(value="menu_level")
	private String menuLevel;
	
	/**
	 * 菜单类型  0 模块 1菜单 2按钮
	 */
	@TableField(value="menu_type")
	private String menuType;
	
	
	/**
	 * 菜单展开状态
	 */
	@TableField(value="menu_state")
	private String state;
	
	
	/**
	 * 菜单路径
	 */
	@TableField(value="menu_url")
	private String url;
	
	/**
	 * 菜单图标
	 */
	@TableField(value="menu_icon")
	private String iconCls;
	
	/**
	 * 排序
	 */
	@TableField(value="menu_sort")
	private Integer menuSort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuLevel() {
		return menuLevel;
	}

	public void setMenuLevel(String menuLevel) {
		this.menuLevel = menuLevel;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public Integer getMenuSort() {
		return menuSort;
	}

	public void setMenuSort(Integer menuSort) {
		this.menuSort = menuSort;
	}
	
	
}
