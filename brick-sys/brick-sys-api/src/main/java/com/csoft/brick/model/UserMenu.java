package com.csoft.brick.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @author Hejeo
 * 用户和功能菜单关系表
 */
@TableName(value = "t_sys_user_menu")
public class UserMenu implements Serializable{
	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 主键
	 */
	@TableId(type = IdType.ID_WORKER)
	private String id;
	
	
	/**
	 * 用户id
	 */
	@TableField(value="user_id")
	private String userId;
	
	/**
	 * 功能菜单id
	 */
	@TableField(value="menu_id")
	private String menuId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
}

