package com.csoft.brick.provider;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.csoft.brick.model.RoleMenu;

public interface IRoleMenuService extends IService<RoleMenu> {
	/**
	 * 根据角色id获取的功能菜单集合
	 * @param roleId 角色id
	 * @return
	 */
	public List<RoleMenu> listRoleMenus(String roleId);
	
	/** 添加或删除角色功能菜单
	 * @param roleMenus 角色功能Json数组集合
	 * @param roleId 角色id
	 */
	public void addOrDelRoleMenus(String roleMenus,String roleId);
}
