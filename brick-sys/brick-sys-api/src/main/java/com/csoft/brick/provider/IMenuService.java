package com.csoft.brick.provider;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.csoft.brick.model.Menu;
import com.csoft.brick.model.MenuTree;

/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public interface IMenuService extends IService<Menu>{
	
	
	
	public List<MenuTree> getMenuListByLevelId(String levelId);
	
	public List<MenuTree> getMenuListByPMenuId(String pMenuId);
	
	
	/**
	 * 展示功能菜单列表
	 * @return
	 */
	public Map<String,Object> listMenu(String menuName);
	
	/**
	 * 展示功能菜单列表
	 * @return
	 */
	public Map<String,Object> listMenuNew(String menuName);
	
	public void addMenu(Menu menu);
	
	public void delMenu(String id);
	
	public Menu getMenu(String id);
	
	public void editMenu(Menu menu);
	
	
	/**
	 * 根据用户编号、上级编号、菜单层级获取菜单集合
	 * @param userId	用户编号
	 * @param pMenuId	上级编号
	 * @param menuLevel 菜单层级
	 * @return
	 */
	public List<Menu> getMenuListByUserIdAndPMenuIdAndMenuLevel(String userId,String pMenuId,String menuLevel); 
	
	
	/** 根据用户编号、上级编号获取菜单集合
	 * @param userId	用户编号
	 * @param pMenuId	上级编号
	 * @return
	 */
	public List<MenuTree> getMenuListByUserIdAndPMenuId(String userId,String pMenuId);
 }
