package com.csoft.brick.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @author Hejeo
 * 用户角色关联表
 */
@TableName(value = "t_sys_user_role")
public class UserRole implements Serializable{
	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 主键
	 */
	@TableId(type = IdType.ID_WORKER)
	private String id;
	
	
	/**
	 * 用户id
	 */
	@TableField(value="user_id")
	private String userId;
	
	/**
	 * 角色id
	 */
	@TableField(value="role_id")
	private String roleId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
}

