package com.csoft.brick.provider;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.csoft.brick.model.UserMenu;

public interface IUserMenuService extends IService<UserMenu> {
	/**
	 * 根据用户id获取用户的功能菜单集合
	 * @param userId 用户id
	 * @return
	 */
	public List<UserMenu> listUserMenus(String userId);
	
	/** 添加或删除用户功能菜单
	 * @param userMenus 用户功能Json数组集合
	 * @param userId 用户id
	 */
	public void addOrDelUserMenus(String userMenus,String userId);
}
