package com.csoft.brick.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @author Hejeo
 * 用户信息表
 */
@TableName(value = "t_sys_user")
public class User implements Serializable{

	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 主键
	 */
	@TableId(type = IdType.ID_WORKER)
	private String id;
	
	
	/**
	 * 用户帐号
	 */
	@TableField(value="user_account")
	private String userAccount;
	
	
	/**
	 * 用户密码
	 */
	@TableField(value="user_password")
	private String userPassword;
	
	
	/**
	 * 用户姓名
	 */
	@TableField(value="user_name")
	private String userName;
	
	
	/**
	 * 手机号
	 */
	@TableField(value="user_mobile")
	private String userMobile;
	
	/**
	 * 电子邮箱
	 */
	@TableField(value="user_email")
	private String userEmail;
	
	
	/**
	 * 用户头像
	 */
	@TableField(value="user_image")
	private String userImage;
	
	/**
	 * 创建时间
	 */
	@TableField(value="create_time")
	private String createTime;
	
	/**
	 * 登录时间
	 */
	@TableField(value="login_time")
	private String loginTime;
	
	/**
	 * 上次登录时间
	 */
	@TableField(value="last_login_time")
	private String lastLoginTime;
	
	/**
	 * 登录次数
	 */
	@TableField(value="login_count")
	private int loginCount;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
}
