package com.csoft.brick.provider;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.csoft.brick.model.User;

public interface IUserService extends IService<User> {
	/**
	 *  获取用户列表
	 * @param userName 用户姓名
	 * @param page 当前页
	 * @param rows 一页多少条数据
	 * @return
	 */
	public Map<String,Object> listUser(String userName,String page,String rows);
	
	/**
	 * 添加用户
	 * @param user 用户对象
	 */
	public void addUser(User user);
	
	
	/**
	 * 根据用户Id删除用户信息
	 * @param id 用户id
	 */
	public void delUser(String id);
	
	
	/** 根据用户id获取用户信息
	 * @param id 用户id
	 * @return
	 */
	public User getUser(String id);
	
	/**
	 * 更新用户信息
	 * @param user
	 */
	public void editUser(User user);
	
	
	/**
	 * 用户登录
	 * @param userAccount 用户帐号
	 * @param userPassword 用户密码
	 * @return 用户对象
	 */
	public User userLogin(String userAccount,String userPassword);
}
