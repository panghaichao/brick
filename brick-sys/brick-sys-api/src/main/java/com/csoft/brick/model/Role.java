package com.csoft.brick.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
/**
 * @author Hejeo
 * 用户和功能菜单关系表
 */
@TableName(value = "t_sys_role")
public class Role implements Serializable{

	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	/**
	 * 主键
	 */
	@TableId(type = IdType.ID_WORKER)
	private String id;
	
	/**
	 * 角色名称
	 */
	@TableField(value="role_name")
	private String roleName;
	
	
	/**
	 * 角色描述
	 */
	@TableField(value="role_desc")
	private String roleDesc;
	
	
	/**
	 * 创建时间
	 */
	@TableField(value="create_time")
	private String createTime;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getRoleName() {
		return roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	public String getRoleDesc() {
		return roleDesc;
	}


	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}


	public String getCreateTime() {
		return createTime;
	}


	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
