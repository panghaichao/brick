package com.csoft.brick.provider;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.csoft.brick.model.Role;
public interface IRoleService extends IService<Role>{
	/**
	 *  获取角色列表
	 * @param roleName 角色名
	 * @param page 当前页
	 * @param rows 一页多少条数据
	 * @return
	 */
	public Map<String,Object> listRole(String roleName,String page,String rows);
	
	/**
	 * 添加角色
	 * @param role 角色对象
	 */
	public void addRole(Role role);
	
	/**
	 * 根据角色Id删除角色信息
	 * @param id 角色id
	 */
	public void delRole(String id);
	
	
	/** 根据角色id获取角色信息
	 * @param id 角色id
	 * @return
	 */
	public Role getRole(String id);
	
	/**
	 * 更新角色信息
	 * @param role 角色对象
	 */
	public void editRole(Role role);
	
	
	public List<Role> getRoleList();
}
