package com.csoft.brick.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * @author Hejeo
 * 功能菜单表
 */
@TableName(value = "t_sys_menu")
public class Menu implements Serializable{

	/**
	 * 
	 */
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 主键
	 */
	@TableId(type = IdType.ID_WORKER)
	private String id;
	
	/**
	 * 上级菜单编号
	 */
	@TableField(value="p_menu_id")
	private String pMenuId;
	
	/**
	 * 菜单名称
	 */
	@TableField(value="menu_name")
	private String menuName;
	
	/**
	 * 菜单编码
	 */
	@TableField(value="menu_code")
	private String menuCode;
	
	/**
	 * 菜单层级
	 */
	@TableField(value="menu_level")
	private String menuLevel;
	
	/**
	 * 菜单类型  0 模块 1菜单 2按钮
	 */
	@TableField(value="menu_type")
	private String menuType;
	
	
	/**
	 * 菜单展开状态
	 */
	@TableField(value="menu_state")
	private String menuState;
	
	
	/**
	 * 菜单路径
	 */
	@TableField(value="menu_url")
	private String menuUrl;
	
	/**
	 * 菜单图标
	 */
	@TableField(value="menu_icon")
	private String menuIcon;
	
	/**
	 * 排序
	 */
	@TableField(value="menu_sort")
	private Integer menuSort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpMenuId() {
		return pMenuId;
	}

	public void setpMenuId(String pMenuId) {
		this.pMenuId = pMenuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuLevel() {
		return menuLevel;
	}

	public void setMenuLevel(String menuLevel) {
		this.menuLevel = menuLevel;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getMenuState() {
		return menuState;
	}

	public void setMenuState(String menuState) {
		this.menuState = menuState;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}

	public Integer getMenuSort() {
		return menuSort;
	}

	public void setMenuSort(Integer menuSort) {
		this.menuSort = menuSort;
	}

}
