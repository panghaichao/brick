package com.csoft.brick.common.properties;

import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * @author Hejeo
 *
 * PropertyPlaceholderConfigurer
 * 是个bean工厂后置处理器的实现，也就是 BeanFactoryPostProcessor接口的一个实现。
 * PropertyPlaceholderConfigurer可以将上下文（配置文 件）中的属性值放在另一个单独的标准java Properties文件中去。
 * 在XML文件中用${key}替换指定的properties文件中的值。这样的话，只需要对properties文件进 行修改，而不用对xml配置文件进行修改。
 */
public class BrickProperties extends PropertyPlaceholderConfigurer{
	
	private static Map<String, String> propertiesMap = new HashMap<String,String>();

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess,Properties props) throws BeansException{
		super.processProperties(beanFactoryToProcess, props);
		for(Object key : props.keySet()){
			String keyStr = key.toString();
			String value = props.getProperty(keyStr);
			propertiesMap.put(keyStr,value);
		}
	}
	
	public static String getString(String key){
		try{
			return propertiesMap.get(key);
		}catch(MissingResourceException e){
			return null;
		}
	}

	public static int getInt(String key){
		return Integer.parseInt(propertiesMap.get(key));
	}
	
}
