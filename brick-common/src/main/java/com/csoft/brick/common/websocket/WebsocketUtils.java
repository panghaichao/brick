package com.csoft.brick.common.websocket;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 *  websocket 调用类
 * @author Hejeo
 *
 */
public class WebsocketUtils {
	protected final static Logger logger = LogManager.getLogger(WebsocketUtils.class);
	
	private static ConcurrentHashMap<String,WebSocketSession> webSocketSessionMap = new ConcurrentHashMap<String,WebSocketSession>();
	
	/**
     * 保存一个连接
     * @param userid
     * @param session
     */
    public static void add(String userid, WebSocketSession session){
    	if(WebsocketUtils.hasConnection(userid)){
    		logger.error("websocket 建立连接" + userid + "已存在");
    		WebsocketUtils.remove(userid);
		}
    	webSocketSessionMap.put(getKey(userid), session);
    }
    
    
    /**
     * 获取一个连接
     * @param userid
     * @return
     */
    public static WebSocketSession get(String userid){
        return webSocketSessionMap.get(getKey(userid));
    }
    
    
    /**
     * 移除一个连接
     * @param userid
     * @throws IOException 
     */
    public static void remove(String userid){
    	
    	WebSocketSession osession = webSocketSessionMap.get(userid);
    	if(osession!=null){
    		try {
				osession.close(new CloseStatus(4000,"关闭"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	webSocketSessionMap.remove(getKey(userid));
    }
    
    
    /**
     * 组装sessionId
     * @param userid
     * @return
     */
    public static String getKey(String userid) {
        return userid;
    }
    
    /**
     * 判断是否有效连接
     * 判断是否存在
     * 判断连接是否开启
     * 无效的进行清除
     * @param userid
     * @return
     */
    public static boolean hasConnection(String userid) {
        String key = getKey(userid);
        if (webSocketSessionMap.containsKey(key)) {
            return true;
        }
        return false;
    }
    
    
    /**
     * 获取连接数的数量
     * @return
     */
    public static int getSize() {
        return webSocketSessionMap.size();
    }
    
    /**
     * 发送消息到客户端
     * @param userid
     * @param message
     * @throws Exception
     */
    public static void sendMessage(String userid, String message){
        if (!hasConnection(userid)) {
        	logger.error("[{}] connection does not exist",getKey(userid));
            throw new NullPointerException(getKey(userid) + " connection does not exist");
        }
        WebSocketSession session = get(userid);
        try {
            session.sendMessage(new TextMessage(message));
        } catch (IOException e) {
        	remove(getKey(userid));
        }
    }
}
