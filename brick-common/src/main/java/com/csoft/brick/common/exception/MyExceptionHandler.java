package com.csoft.brick.common.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class MyExceptionHandler implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {

		if (!(request.getHeader("accept").indexOf("application/json") > -1
				|| (request.getHeader("X-Requested-With") != null
						&& request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {
			
			// 如果不是异步请求
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("ex", ex.getMessage());

			if (ex instanceof BusinessException) {
				return new ModelAndView("error-business", model);
			} else if (ex instanceof ParameterException) {
				return new ModelAndView("error", model);
			} else {
				return new ModelAndView("error", model);
			}
		} else {// JSON格式返回
			try {
				PrintWriter writer = response.getWriter();
				writer.write(ex.getMessage());
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;

		}

	}

}
