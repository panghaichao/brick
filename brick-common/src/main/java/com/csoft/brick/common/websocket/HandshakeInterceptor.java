package com.csoft.brick.common.websocket;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;


/**
 * 功能说明：websocket连接的拦截器
 * 有两种方式
 *          一种是实现接口HandshakeInterceptor，实现beforeHandshake和afterHandshake函数
 *          一种是继承HttpSessionHandshakeInterceptor，重载beforeHandshake和afterHandshake函数
 * 我这里是参照spring官方文档中的继承HttpSessionHandshakeInterceptor的方式
 */
public class HandshakeInterceptor extends HttpSessionHandshakeInterceptor {
	protected final Logger logger = LogManager.getLogger(HandshakeInterceptor.class);
	
	
	/**
     * 从请求中获取唯一标记参数，填充到数据传递容器attributes
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @param wsHandler
     * @param attributes
     * @return
     * @throws Exception
     */
	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Map<String, Object> attributes) throws Exception {
		
		if(request.getHeaders().containsKey("Sec-WebSocket-Extensions")){
            request.getHeaders().set("Sec-WebSocket-Extensions","permessage-deflate");  
        }
		
		if(request instanceof ServletServerHttpRequest){
            ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
            HttpServletRequest hrequest = servletRequest.getServletRequest();
            
            // 获取用户id
            String userid = hrequest.getParameter("userid");
            if(userid != null && !"".equals(userid.trim())){
            	logger.info("websocket 握手[userid=" + userid + "]");
            	// 根据用户id从数据库查询用户信息
//            	SA04 user = coreService.getUserByID(userid);
            	attributes.put("userid", userid);
            	
            }else{
            	logger.error("websocket 握手[userid is null]");
            	return false;
            }
        }
		super.beforeHandshake(request, response, wsHandler, attributes);
        return true;
	}

	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Exception ex) {
		
		logger.info("After Handshake");
		super.afterHandshake(request, response, wsHandler, ex);
	}
}
