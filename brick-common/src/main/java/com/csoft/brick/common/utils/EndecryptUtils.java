package com.csoft.brick.common.utils;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * shiro进行加密解密的工具类封装 
 * @author Hejeo
 * 2017年6月1日 15:06:07
 */
public class EndecryptUtils{
	/** 
     * base64进制加密 
     * @param str 要加密的字符串 
     * @return 加密后的字符串
     */ 
    public static String encrytBase64(String str) { 
        byte[] bytes = str.getBytes(); 
        return Base64.encodeToString(bytes); 
    }
    
    /** 
     * base64进制解密 
     * @param str 要解密的字符串
     * @return 解密后的字符串
     */ 
    public static String decryptBase64(String str) { 
        return Base64.decodeToString(str); 
    }
    
    /** 
     * 16进制加密 
     * @param str 要加密的字符串 
     * @return 加密后的字符串
     */ 
    public static String encrytHex(String str) { 
        byte[] bytes = str.getBytes(); 
        return Hex.encodeToString(bytes); 
    } 
    
    /** 
     * 16进制解密 
     * @param str 要解密的字符串
     * @return 解密后的字符串
     */ 
    public static String decryptHex(String str) { 
        return new String(Hex.decode(str)); 
    }
    
    
    /** 
     * MD5加密 
     * @param str 要加密的字符串 
     * @param salt 加密盐
     * @return 加密后的字符串
     */ 
    public static String encrytMD5(String str,String salt) { 
    	return new Md5Hash(str, salt).toString();
    }
    
    
    public static void main(String[] args) {
    	SecureRandomNumberGenerator secureRandomNumberGenerator=new SecureRandomNumberGenerator(); 
        String salt= secureRandomNumberGenerator.nextBytes().toHex(); 
        System.out.println(salt);
        
        String pass = "123456";
        System.out.println(EndecryptUtils.encrytMD5(pass, "cctc"));
	}
    
    
    
    
}
