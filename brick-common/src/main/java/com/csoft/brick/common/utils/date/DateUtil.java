package com.csoft.brick.common.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	private static SimpleDateFormat shortDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	
	private static SimpleDateFormat longDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static String PATTERN = "yyyy-MM-dd";
	
	/**
	 *  获取当前时间  yyyyMMddHHmmssSSS
	 * @return 20170512134817
	 */
	public static String getShortDateTime(){
		return shortDateFormat.format(new Date());
	}
	
	/**
	 *  获取当前时间  yyyy-MM-dd HH:mm:ss
	 * @return 2017-05-12 13:48:17
	 */
	public static String getLongDateTime(){
		return longDateFormat.format(new Date());
	}
	
	public static Date formatLongStrToDate(String dateStr) throws ParseException{
		return longDateFormat.parse(dateStr);
	}
	
	public static Date formatShortStrToDate(String dateStr) throws ParseException{
		return shortDateFormat.parse(dateStr);
	}
	
	public static long getCurrentDate(){
		return System.currentTimeMillis();
	}
	
	//获取到小时(整点)的时间戳 单位 毫秒  
//	public static long getCurrentDateHour(){
//		Calendar calendar = Calendar.getInstance();
//		calendar.set(Calendar.MINUTE,0);
//		calendar.set(Calendar.SECOND,0);
//		calendar.set(Calendar.MILLISECOND,0);
//		return calendar.getTimeInMillis();
//	}
	
	//获取时间戳 单位 毫秒  
	public static long getCurrentDateHourMinute(int minuteDiff){
		Calendar calendar = Calendar.getInstance();
		int minute = calendar.get(Calendar.MINUTE);
		minute = (int)Math.ceil(minute / minuteDiff) * minuteDiff;
		calendar.set(Calendar.MINUTE,minute);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTimeInMillis();
	}
	
	
	//获取时间戳 单位 毫秒  
	public static long getCurrentDateHourMinute(long millis,int minuteDiff){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		int minute = calendar.get(Calendar.MINUTE);
		minute = (int)Math.ceil(minute / minuteDiff) * minuteDiff;
		calendar.set(Calendar.MINUTE,minute);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTimeInMillis();
	}
	
	
	//格式化日期
	public static String format(Object date){
		return format(date,PATTERN);
	}

	//格式化日期
	public static String format(Object date,String pattern){
		if(date == null){
			return null;
		}
		if(pattern == null){
			return format(date);
		}
		return new SimpleDateFormat(pattern).format(date);
	}
	
	//判断是否是工作日
	public static boolean isWorkDay(){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		int dayofweek = calendar.get(Calendar.DAY_OF_WEEK);
		if(Calendar.SATURDAY == dayofweek || Calendar.SUNDAY == dayofweek){
			return false;
		}else{
			return true;
		}
	}
	
	//判断是否是周末
	public static boolean isWeekend(){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		int dayofweek = calendar.get(Calendar.DAY_OF_WEEK);
		if(Calendar.SATURDAY == dayofweek || Calendar.SUNDAY == dayofweek){
			return true;
		}else{
			return false;
		}
	}
	
	//根据当前日期时间（精确毫秒）获取时间 返回 毫秒
	public static long getTimeCurrentMillis(){
		return getTimeMillis(System.currentTimeMillis());
	}
	
	//根据指定日期时间（精确毫秒）获取时间 返回 毫秒
	public static long getTimeMillis(long datatime){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.setTimeInMillis(datatime);
		int hour = mCalendar.get(Calendar.HOUR_OF_DAY);
		int minute = mCalendar.get(Calendar.MINUTE);
		int second = mCalendar.get(Calendar.SECOND);
		return (hour * 3600 + minute * 60 + second) * 1000;
	}
	
	//根据指定年月获取当月第一天的日期时间
	public static long getMonthFirstDayDateTime(int year,int month){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.set(year,month,1,0,0,0);
		mCalendar.set(Calendar.MILLISECOND,0);
		return mCalendar.getTimeInMillis();
	}
	
	//根据指定年月获取当月最后一天的日期时间
	public static long getMonthLastDayDateTime(int year,int month){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.set(Calendar.YEAR,year);
		mCalendar.set(Calendar.MONTH,month);
		int lastday = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		mCalendar.set(year,month,lastday,23,59,59);
		mCalendar.set(Calendar.MILLISECOND,999);
		return mCalendar.getTimeInMillis();
	}
	
	//获取当天的零点时刻
	public static long getDayFirstDateTime(){
		return getDayFirstDateTime(System.currentTimeMillis());
	}
	
	//获取指定时间的零点时刻
	public static long getDayFirstDateTime(long datetime){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.setTimeInMillis(datetime);
		int year = mCalendar.get(Calendar.YEAR);
		int month = mCalendar.get(Calendar.MONTH);
		int day = mCalendar.get(Calendar.DAY_OF_MONTH);
		return getDayFirstDateTime(year,month,day);
	}
	
	//获取指定年月日的零点时刻
	public static long getDayFirstDateTime(int year,int month,int day){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.set(year,month,day,0,0,0);
		mCalendar.set(Calendar.MILLISECOND,0);
		return mCalendar.getTimeInMillis();
	}
	
	//获取当天的23 59 59时刻
	public static long getDayLastDateTime(){
		return getDayLastDateTime(System.currentTimeMillis());
	}
		
	//获取指定时间的23 59 59时刻
	public static long getDayLastDateTime(long datetime){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.setTimeInMillis(datetime);
		int year = mCalendar.get(Calendar.YEAR);
		int month = mCalendar.get(Calendar.MONTH);
		int day = mCalendar.get(Calendar.DAY_OF_MONTH);
		return getDayLastDateTime(year,month,day);
	}
	
	//获取指定年月日的23 59 59时刻
	public static long getDayLastDateTime(int year,int month,int day){
		Calendar mCalendar = Calendar.getInstance();
		mCalendar.set(year,month,day,23,59,59);
		mCalendar.set(Calendar.MILLISECOND,999);
		return mCalendar.getTimeInMillis();
	}
	
	/**
	 * 将时刻转化为毫秒数
	 * @param time
	 * @return
	 */
	public static long timeToSec(String time){
		String[] my = time.split(":");
		int hour = Integer.parseInt(my[0]);
		int min = Integer.parseInt(my[1]);
		int sec = Integer.parseInt(my[2]);
		long totalSec = (hour * 3600 + min * 60 + sec) * 1000;
		return totalSec;
	}
	
	/**
	 * 将毫秒数转化为时刻
	 * @param totalSec
	 * @return
	 */
	public static String secToTime(long totalSec){
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
		String hms = formatter.format(totalSec);
		return hms;
	}
	
	/**
	 * 获取当前时间前一小时的系统时间
	 * @return
	 */
	public static Long getPrevSystemTime(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MINUTE,0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
		return calendar.getTimeInMillis();
	}
	
	
	public static void main(String[] args) {
		System.out.println(DateUtil.getShortDateTime());
		System.out.println(DateUtil.getLongDateTime());
	}
}
