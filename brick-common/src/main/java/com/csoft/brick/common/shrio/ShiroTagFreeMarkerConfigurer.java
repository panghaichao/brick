package com.csoft.brick.common.shrio;

import java.io.IOException;

import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.csoft.brick.common.shrio.tag.ShiroTags;

import freemarker.template.TemplateException;

/**
 * shiro 支持FreeMarker 配置
 * @author Hejeo
 *
 */
public class ShiroTagFreeMarkerConfigurer extends FreeMarkerConfigurer{
	@Override
    public void afterPropertiesSet() throws IOException, TemplateException {
        super.afterPropertiesSet();
        this.getConfiguration().setSharedVariable("shiro", new ShiroTags());
    }
}
