package com.csoft.brick.common.redis;

public class Pair<K, V>{
	
	private K key;
	
	private V value;
	
	public Pair(K key,V value){
		this.key = key;
		this.value = value;
    }  

	public K getKey(){
		return key;
	}
	
	public void setKey(K key){
		this.key = key;
	}  

	public V getValue(){
		return value;
	}
	
	public void setValue(V value){
        this.value = value;
	}
	
	public static <K,V> Pair<K, V> makePair(K key, V value){
		return new Pair<K, V>(key, value);
	}
	
}  
