package com.csoft.brick.common.qiniu;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

public class QiniuSimpleUpload {
	Auth auth = Auth.create(AccountMgr.ACCESS_KEY, AccountMgr.SECRET_KEY);
	
	UploadManager uploadManager = new UploadManager(new Configuration());
    
    /**
     * 获取凭证
     * @param bucketName 空间名称
     * @return
     */
    public String getUpToken(String bucketName) {
        return auth.uploadToken(bucketName);
    }
    
    
    /**
     * 上传
     * @param filePath 文件路径  （也可以是字节数组、或者File对象）
     * @param key 上传到七牛上的文件的名称  （同一个空间下，名称【key】是唯一的）
     */
    public void upload(String filePath, String key) {
        try {
            // 调用put方法上传
//            Response res = uploadManager.put(filePath, key, getUpToken(AccountMgr.BUCKET_KEY));
        	uploadManager.put(filePath, key, getUpToken(AccountMgr.BUCKET_KEY));
            // 打印返回的信息
//            System.out.println(res.bodyString());
        } catch (QiniuException e) {
            Response r = e.response;
            // 请求失败时打印的异常的信息
            System.out.println(r.toString());
            try {
                // 响应的文本信息
                System.out.println(r.bodyString());
            } catch (QiniuException qe) {
            }
        }
    }
    
    
    /**
     * 主函数：程序入口，测试功能
     * @param args
     */
    public static void main(String[] args) {
        // 上传文件的路径，因为在Mac下，所以路径和windows下不同
        String filePath = "D:/1.png";
        // 上传到七牛后保存的文件名
        String key = "2.png";

        new QiniuSimpleUpload().upload(filePath, key);
    }
}
