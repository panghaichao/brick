package com.csoft.brick.common.freemarker;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

import com.csoft.brick.common.utils.date.DateUtil;

public class MyFreeMarkerView extends FreeMarkerView {
	private static Logger logger = LogManager.getLogger(MyFreeMarkerView.class);
	private static final String CONTEXT_PATH = "basePath";

	@Override
	protected void exposeHelpers(Map<String, Object> model, HttpServletRequest request) throws Exception {
		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int port = request.getServerPort();
		String path = request.getContextPath();
		String basePath = scheme + "://" + serverName + ":" + port + path;
		model.put(CONTEXT_PATH, path);
		logger.info("===========系统路径：" + basePath);
		logger.info("===========系统时间：" + DateUtil.getLongDateTime());
		super.exposeHelpers(model, request);
	}
}
