package com.csoft.brick.common.redis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.csoft.brick.common.redis.Pair;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPipeline;
import redis.clients.jedis.ShardedJedisPool;

@Repository(value = "shardedJedisDAO")
public class ShardedJedisDAO{
	protected final Logger logger = LogManager.getLogger(ShardedJedisDAO.class);
	
	public int EXPIRE = 60 * 60 * 1; //1小时
	
	@Autowired
    private ShardedJedisPool shardedJedisPool;
	
	private ShardedJedis getShardedJedis(){
		return shardedJedisPool.getResource();
	}
	
	private <K> K run(RunExecutor<K> runexecutor){
		ShardedJedis shardedJedis = getShardedJedis();
		if(shardedJedis == null){
			return null;
		}
		try{
			K result = runexecutor.execute(shardedJedis);
			return result;
		}catch(Exception e){
			logger.error("Redis execute exception {}",e);
			throw new RuntimeException("Redis execute exception", e);
		}finally{
			if(shardedJedis != null){
				shardedJedis.close();
			}
		}
	}
	
	/**
	 * 为给定 key 设置生存时间，当 key 过期时(生存时间为 0 )，它会被自动删除。
	 * @param key      key 
	 * @param expire   生命周期，单位为秒 
	 * @return         1:设置成功  0:已经超时或key不存在
	 */
	public Long expire(final String key,final int expire){  
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
				 return jedis.expire(key,expire);  
			}
			
		});
		
	}
	
	/**
	 * ID生成器 
	 * @param key   id的key 
	 * @return      返回生成的ID 
	 */
	public long makeID(final String key){  
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
				long id = jedis.incr(key);
				if((id + 75807) >= Long.MAX_VALUE){
					jedis.getSet(key,"0");
				}
				return id;
			}
			
		});
	}
	
	/* ======================================Start String====================================== */ 
	
	/**
	 * 将字符串值value关联到key
	 * 如果key已经持有其他值，setString 就覆写旧值，无视类型
	 * 对于某个原本带有生存时间（TTL）的键来说， 当 setString 成功在这个键上执行时， 这个键原有的 TTL 将被清除。 
	 * @param key      键 
	 * @param value    值 
	 * @return         在设置操作成功完成时，才返回 OK 。 
	 */
	public String setString(final String key,final String value){ 
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				return jedis.set(key,value); 
			}
			
		});
	}
	
	/**
	 * 将值 value 关联到 key ，并将 key 的生存时间设为 expire (以秒为单位)。 
	 * 如果 key 已经存在， 将覆写旧值。 
	 * @param key     键
	 * @param value   值
	 * @param expire  生命周期 以秒为单位
	 * @return        设置成功时返回 OK
	 */
	public String setString(final String key,final String value,final int expire){  
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				return jedis.setex(key,expire,value);
			}
			
		});
	}
	
	/**
	 * 将 key 的值设为 value ，当且仅当 key 不存在。若给定的 key 已经存在，则 setStringIfNotExists 不做任何动作
	 * @param key    键
	 * @param value  值
	 * @return       设置成功，返回 1 。设置失败，返回 0 。 
	 */
	public Long setStringIfNotExists(final String key,final String value){  
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
				 return jedis.setnx(key,value);  
			}
			
		});
	}
	
	/**
	 * 返回 key 所关联的字符串值。如果 key 不存在那么返回特殊值 nil 。 
	 * @param key
	 * @return
	 */
	public String getString(final String key){  
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				return jedis.get(key);
			}
			
		});
	}
	
	/**
	 * 批量的{@link #setString(String,String)}
	 * @param pairs
	 * @return
	 */
	public List<Object> batchSetString(final List<Pair<String,String>> pairs){
		return run(new RunExecutor<List<Object>>(){
			
			@Override
			public List<Object> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();
				for(Pair<String,String> pair : pairs){
					pipeline.set(pair.getKey(),pair.getValue());
				}
				return pipeline.syncAndReturnAll();
			}
			
		});
	}
	
	/**
	 * 批量的{@link #getString(String)}
	 * @param keys
	 * @return
	 */
	public List<String> batchGetString(final String[] keys){
		
		return run(new RunExecutor<List<String>>(){
			
			@Override
			public List<String> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();
				List<String> result = new ArrayList<String>(keys.length);
				List<Response<String>> responses = new ArrayList<Response<String>>(keys.length);
				for(String key : keys){
					responses.add(pipeline.get(key));
				}
				pipeline.sync();
				for(Response<String> resp : responses){
					result.add(resp.get());
				}
				return result;
			}
			
		});
		
	}
	
	/* ======================================Ended String====================================== */
	
	/* ======================================Start Hashes====================================== */
	
	
	/**
	 * 获取哈希表 key 的长度
	 * @param key
	 * @return
	 */
	public Long hashLength(final String key){ 
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
				return jedis.hlen(key);
			}
			
		});
	}
	
	
	/**
	 * 获取 哈希表 key 域集合
	 * @param key
	 * @return
	 */
	public List<String> hashKeys(final String key){ 
		return run(new RunExecutor<List<String>>(){
			
			@Override
			public List<String> execute(ShardedJedis jedis){
				List<String> list = new ArrayList<String>(jedis.hkeys(key));
				return list;
			}
			
		});
	}
	
	
	/**
	 * 获取 哈希表 key 值集合
	 * @param key
	 * @return
	 */
	public List<String> hashValues(final String key){ 
		return run(new RunExecutor<List<String>>(){
			
			@Override
			public List<String> execute(ShardedJedis jedis){
				return jedis.hvals(key);
			}
			
		});
	}
	
	/**
	 * 删除 哈希表 key 的域
	 * @param key
	 * @param fields
	 * @return
	 */
	public Long hashDelete(final String key,final String... fields){ 
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
				
				return jedis.hdel(key,fields);
			}
			
		});
	}
	
	/**
	 * 将哈希表 key 中的域 field 的值设为 value 。 
	 * 如果 key 不存在，一个新的哈希表被创建并进行 hashSet 操作。 
     * 如果域 field 已经存在于哈希表中，旧值将被覆盖。 
	 * @param key    键
	 * @param field  域 
	 * @param value  值
	 * @return       如果 field 是哈希表中的一个新建域，并且值设置成功，返回 1 。如果哈希表中域 field 已经存在且旧值已被新值覆盖，返回 0 。 
	 */
	public Long hashSet(final String key,final String field,final String value){ 
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
				 return jedis.hset(key,field,value); 
			}
			
		});
	}
	
	/**
	 * 将哈希表 key 中的域 field 的值设为 value 。 
     * 如果 key 不存在，一个新的哈希表被创建并进行 hashSet 操作。 
     * 如果域 field 已经存在于哈希表中，旧值将被覆盖。 
	 * @param key       键
	 * @param field     域
	 * @param value     值
	 * @param expire    生命周期，单位为秒 
	 * @return          如果 field 是哈希表中的一个新建域，并且值设置成功，返回 1 。如果哈希表中域 field 已经存在且旧值已被新值覆盖，返回 0 。 
	 */
	public Long hashSet(final String key,final String field,final String value,final int expire){  
		return run(new RunExecutor<Long>(){
			
			@Override
			public Long execute(ShardedJedis jedis){
			    Pipeline pipeline = jedis.getShard(key).pipelined();  
                Response<Long> result = pipeline.hset(key,field,value);
                pipeline.expire(key,expire);  
                pipeline.sync();
                return result.get();
			}
			
		});
	}
	
	/**
	 * 返回哈希表 key 中给定域 field 的值。 
	 * @param key     键
	 * @param field   域
	 * @return        给定域的值。当给定域不存在或是给定 key 不存在时，返回 nil 。
	 */
	public String hashGet(final String key,final String field){  
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				return jedis.hget(key,field);
			}
			
		});
	}
	
	/**
	 * 返回哈希表 key 中给定域 field 的值。 如果哈希表 key 存在，同时设置这个 key 的生存时间
	 * @param key         键
	 * @param field       域
	 * @param expire      生命周期，单位为秒
	 * @return            给定域的值。当给定域不存在或是给定 key 不存在时，返回 nil 。 
	 */
	public String hashGet(final String key,final String field,final int expire){  
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				Pipeline pipeline = jedis.getShard(key).pipelined();
                Response<String> result = pipeline.hget(key,field);
                pipeline.expire(key, expire);
                pipeline.sync();
                return result.get();
			}
			
		});
	}
	
	/**
	 * 同时将多个 field-value (域-值)对设置到哈希表 key 中。 
	 * @param key    键
	 * @param hash   域-值 MAP
	 * @return       如果命令执行成功，返回 OK 。当 key 不是哈希表(hash)类型时，返回一个错误。 
	 */
	public String hashMultipleSet(final String key,final Map<String,String> hash){  
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				return jedis.hmset(key,hash);
			}
			
		});
	}
	
	/**
	 * 同时将多个 field-value (域-值)对设置到哈希表 key 中。同时设置这个 key 的生存时间
	 * @param key       键
	 * @param hash      域-值 MAP
	 * @param expire    生命周期，单位为秒 
	 * @return          如果命令执行成功，返回 OK 。当 key 不是哈希表(hash)类型时，返回一个错误。 
	 */
	public String hashMultipleSet(final String key,final Map<String,String> hash,final int expire){  
		return run(new RunExecutor<String>(){
			
			@Override
			public String execute(ShardedJedis jedis){
				 Pipeline pipeline = jedis.getShard(key).pipelined();
				 Response<String> result = pipeline.hmset(key,hash);
				 pipeline.expire(key, expire);
				 pipeline.sync();
				 return result.get();
			}
			
		});
	}
	
	/**
	 * 返回哈希表 key 中，一个或多个给定域的值。如果给定的域不存在于哈希表，那么返回一个 nil 值。 
	 * @param key      键
	 * @param fields   域的数组 
	 * @return          一个包含多个给定域的关联值的表，表值的排列顺序和给定域参数的请求顺序一样
	 */
	public List<String> hashMultipleGet(final String key,final String... fields){  
		return run(new RunExecutor<List<String>>(){
			
			@Override
			public List<String> execute(ShardedJedis jedis){
				return jedis.hmget(key,fields);
			}
			
		});
	}
	
	/**
	 * 返回哈希表 key 中，一个或多个给定域的值。如果给定的域不存在于哈希表，那么返回一个 nil 值。 
     * 同时设置这个 key 的生存时间 
	 * @param key      键
	 * @param expire   生命周期，单位为秒
	 * @param fields   域的数组 
	 * @return
	 */
	public List<String> hashMultipleGet(final String key,final int expire,final String... fields){  
		return run(new RunExecutor<List<String>>(){
			
			@Override
			public List<String> execute(ShardedJedis jedis){
				Pipeline pipeline = jedis.getShard(key).pipelined();
				Response<List<String>> result = pipeline.hmget(key,fields);  
				pipeline.expire(key,expire);
				pipeline.sync();
				return result.get();
			}
			
		});
	}
	
	/**
	 * 批量的{@link #hashMultipleSet(String,Map)}，在管道中执行 
	 * @param pairs
	 * @return
	 */
	public List<Object> batchHashMultipleSet(final List<Pair<String,Map<String,String>>> pairs){  
		return run(new RunExecutor<List<Object>>(){
			
			@Override
			public List<Object> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();  
				for(Pair<String,Map<String,String>> pair : pairs){  
					pipeline.hmset(pair.getKey(),pair.getValue());  
				}
				return pipeline.syncAndReturnAll();    
			}
			
		});
	}
	
	/**
	 * 批量的{@link #hashMultipleSet(String, Map)}，在管道中执行 
	 * @param data
	 * @return
	 */
	public List<Object> batchHashMultipleSet(final Map<String,Map<String,String>> data){  
		return run(new RunExecutor<List<Object>>(){
			
			@Override
			public List<Object> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();
				for(Map.Entry<String,Map<String,String>> iter : data.entrySet()){
					pipeline.hmset(iter.getKey(),iter.getValue()); 
				}
				return pipeline.syncAndReturnAll();  
			}
			
		});
	}
	
	/**
	 * 批量的{@link #hashMultipleGet(String,String...)}，在管道中执行 
	 * @param pairs
	 * @return
	 */
	public List<List<String>> batchHashMultipleGet(final List<Pair<String,String[]>> pairs){  
		return run(new RunExecutor<List<List<String>>>(){
			
			@Override
			public List<List<String>> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();
				List<List<String>> result = new ArrayList<List<String>>(pairs.size());
				List<Response<List<String>>> responses = new ArrayList<Response<List<String>>>(pairs.size());
				for(Pair<String, String[]> pair : pairs){
					responses.add(pipeline.hmget(pair.getKey(),pair.getValue()));
				}
				pipeline.sync();
				for(Response<List<String>> resp : responses){
					result.add(resp.get());
				}
				return result;
			}
			
		});
	}
	
	/**
	 * 返回哈希表 key 中，所有的域和值。在返回值里，紧跟每个域名(field name)之后是域的值(value)，所以返回值的长度是哈希表大小的两倍。 
	 * @param key
	 * @return    以列表形式返回哈希表的域和域的值。若 key 不存在，返回空列表。 
	 */
	public Map<String,String> hashGetAll(final String key){  
		return run(new RunExecutor<Map<String,String>>(){
			
			@Override
			public Map<String,String> execute(ShardedJedis jedis){
				return jedis.hgetAll(key);  
			}
			
		});
	}
	
	/**
	 * 返回哈希表 key 中，所有的域和值。在返回值里，紧跟每个域名(field name)之后是域的值(value)，所以返回值的长度是哈希表大小的两倍 
	 * @param key     键
	 * @param expire  生命周期，单位为秒 
	 * @return        以列表形式返回哈希表的域和域的值。若 key 不存在，返回空列表
	 */
	public Map<String,String> hashGetAll(final String key, final int expire){  
		return run(new RunExecutor<Map<String,String>>(){
			
			@Override
			public Map<String,String> execute(ShardedJedis jedis){
				Pipeline pipeline = jedis.getShard(key).pipelined();
				Response<Map<String,String>> result = pipeline.hgetAll(key);
				pipeline.expire(key,expire);
				pipeline.sync();
				return result.get();
			}
			
		});
	}
	 
	/**
	 * 批量的{@link #hashGetAll(String)} 
	 * @param keys
	 * @return
	 */
	public List<Map<String,String>> batchHashGetAll(final String... keys){  
		return run(new RunExecutor<List<Map<String,String>>>(){
			
			@Override
			public List<Map<String,String>> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();  
				List<Map<String,String>> result = new ArrayList<Map<String, String>>(keys.length);  
				List<Response<Map<String,String>>> responses = new ArrayList<Response<Map<String,String>>>(keys.length);  
				for(String key : keys){  
					responses.add(pipeline.hgetAll(key));  
				}  
				pipeline.sync();  
				for(Response<Map<String,String>> resp : responses){  
					result.add(resp.get());  
				}  
				return result;  
			}
		});
	}
	
	/**
	 * 批量的{@link #hashMultipleGet(String, String...)}，与{@link #batchHashGetAll(String...)}不同的是，返回值为Map类型 
	 * @param keys
	 * @return
	 */
	public Map<String, Map<String, String>> batchHashGetAllForMap(final String... keys){  
		return run(new RunExecutor<Map<String,Map<String,String>>>(){
			
			@Override
			public Map<String,Map<String,String>> execute(ShardedJedis jedis){
				ShardedJedisPipeline pipeline = jedis.pipelined();
				//设置map容量防止rehash
                int capacity = 1;
                while((int)(capacity * 0.75) <= keys.length){
                	capacity <<= 1;
                }
                Map<String,Map<String,String>> result = new HashMap<String,Map<String,String>>(capacity);
                List<Response<Map<String,String>>> responses = new ArrayList<Response<Map<String,String>>>(keys.length);
                for(String key : keys){
                    responses.add(pipeline.hgetAll(key));
                }
                pipeline.sync();
                for(int i = 0;i < keys.length;++i){
                    result.put(keys[i],responses.get(i).get());
                }
                return result;
			}
		});
	}
	
	/* ======================================Ended Hashes====================================== */
	
	
	/* ======================================Start   List====================================== */ 
	
	/* ======================================Ended   List====================================== */ 
	
	static interface RunExecutor<K>{
		public K execute(ShardedJedis jedis);
	}
	
}
