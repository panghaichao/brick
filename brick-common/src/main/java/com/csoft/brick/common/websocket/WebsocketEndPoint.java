package com.csoft.brick.common.websocket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.alibaba.fastjson.JSONObject;
import com.csoft.brick.common.utils.date.DateUtil;

public class WebsocketEndPoint extends TextWebSocketHandler{
	
	protected final Logger logger = LogManager.getLogger(WebsocketEndPoint.class);
	
//	private static ConcurrentHashMap<String,WebSocketSession> webSocketSessionMap = new ConcurrentHashMap<String,WebSocketSession>();
	
	/**
     * 建立连接
     * @param session
     * @throws Exception
     */
	@Override
	public void afterConnectionEstablished(WebSocketSession session)throws Exception{
		String userid = (String) session.getAttributes().get("userid");
		logger.info("websocket 建立连接[" + session.getId() + "," + userid + "]");
        WebsocketUtils.add(userid, session);
	}
	
	
	/**
     * 收到客户端消息
     * @param session
     * @param message
     * @throws Exception
     */
	@Override  
    protected void handleTextMessage(WebSocketSession session,TextMessage message) throws Exception {  
        String userid = (String) session.getAttributes().get("userid");
        logger.info(DateUtil.getLongDateTime()+" 收到客户端 ["+userid+"] 发来的消息：["+message.getPayload()+"]");
        JSONObject json = new JSONObject();
		json.put("notice_content", message.getPayload());
        WebsocketUtils.sendMessage(userid,json.toJSONString());
    }
	
	
	/**
     * 出现异常
     * @param session
     * @param exception
     * @throws Exception
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
    	String userid = (String) session.getAttributes().get("userid");
    	logger.info("websocket 连接[" + session.getId() + "," + userid + "] 出现异常！");
    	WebsocketUtils.remove(userid);
    }
	
    /**
     * 连接关闭
     * @param session
     * @param closeStatus
     * @throws Exception
     */
	@Override
	public void afterConnectionClosed(WebSocketSession session,CloseStatus status)throws Exception{
		if(status.getCode() == 4000){
			return;
		}
		
		String userid = (String) session.getAttributes().get("userid");
		WebSocketSession msession = WebsocketUtils.get(userid);
		if(session != null && session.getId().equals(msession.getId())){
			logger.info("websocket 断开连接[" + session.getId() + "," + userid + "," + status + "]");
			WebsocketUtils.remove(userid);
		}else{
			logger.error("websocket 断开连接[userid is null ]");
		}
		
	}
	
	
//	
//	public boolean sendMessage(String message){
//		try{
//			
//			String key = "Brick";
//			WebSocketSession wssession = webSocketSessionMap.get(key);
//			
//			if(wssession != null){
//				wssession.sendMessage(new TextMessage(message));
//			}
//			return true;
//		}catch(Exception e){
//			return false;
//		}
//	}
	
	/**
	 * 给SA04用户自己以外的用户发送消息
	 * */
//	public boolean sendMessageToOther(SA04 user,String message){
//		try {
//			for (WebSocketSession wssession : webSocketSessionMap.values()) {
//				SA04 touser = (SA04) wssession.getAttributes().get(GiMACConstants.CURRENT_USER);
//				if (touser != null&& !touser.getSAD001().equals(user.getSAD001())) {
//					wssession.sendMessage(new TextMessage(message));
//					try{
//						Thread.sleep(1);
//					}catch(Exception e){
//					}
//				}
//			}
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
	
//	public boolean sendAll(String message){
//		try{
//			for(WebSocketSession wssession : webSocketSessionMap.values()){
//				wssession.sendMessage(new TextMessage(message));
//				try{
//					Thread.sleep(1);
//				}catch(Exception e){
//				}
//			}
//			return true;
//		}catch(Exception e){
//			return false;
//		}
//	} 
}
