package com.csoft.brick.common.redis;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;


public class ObjectRedisSerializer implements RedisSerializer<Object>{
	protected final Logger logger = LogManager.getLogger(ObjectRedisSerializer.class);
	
	@Override
	public Object deserialize(byte[] bytes)throws SerializationException{
		if(bytes == null || bytes.length == 0){
			return null;
		}
		ByteArrayInputStream byteArrayInputStream = null;
		ObjectInputStream objectInputStream = null;
		try{
			byteArrayInputStream = new ByteArrayInputStream(bytes);
			objectInputStream = new ObjectInputStream(byteArrayInputStream);
			return objectInputStream.readObject();
		}catch(Exception ex){
			ex.printStackTrace();
			logger.error("Cannot deserialize {}",ex);
			throw new SerializationException("Cannot deserialize", ex);
		}finally{
			if(objectInputStream != null){
				try{
					objectInputStream.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			if(byteArrayInputStream != null){
				try{
					byteArrayInputStream.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public byte[] serialize(Object object)throws SerializationException{
		if(object == null){
			return new byte[0];
		}
		ByteArrayOutputStream byteArrayOutputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try{
			byteArrayOutputStream = new ByteArrayOutputStream();
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			return byteArrayOutputStream.toByteArray();
		}catch(Exception ex){
			ex.printStackTrace();
			logger.error("Cannot serialize {}",ex);
			throw new SerializationException("Cannot serialize", ex);
		}finally{
			if(objectOutputStream != null){
				try{
					objectOutputStream.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			if(byteArrayOutputStream != null){
				try{
					byteArrayOutputStream.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}
	
}
