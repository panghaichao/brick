package com.csoft.brick.common.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogInterceptor {
//	/**
//	 * 这里针对org.lee.service.Imp目录下的新增，修改方法进行记录
//	 */
//	@Pointcut("execution(* com.csoft.brick..*.*(..)) ")
//	public void allMethod() {
//
//	}
//
//	@Before("allMethod()")
//	public void before() {
//	}
//
//	@After("allMethod()")
//	public void afterLog(JoinPoint point) {
//		System.out.println("--------------afterLog:--------------最终通知");
//		System.out.println("--------------afterLog:=========目标方法为：" + point.getSignature().getDeclaringTypeName() + "."
//				+ point.getSignature().getName());
//		System.out.println("--------------afterLog:--------------参数为：" + Arrays.toString(point.getArgs()));
//		System.out.println("--------------afterLog:--------------被织入的对象为：" + point.getTarget());
//
//	}
//
//	@AfterReturning(value = "allMethod()", returning = "returnValue")
//	public void afterRunningLog(JoinPoint point, Object returnValue) {
//		System.out.println("--------------afterRunningLog--------------返回值后通知");
//		System.out.println("--------------afterRunningLog----------------目标方法："
//				+ point.getSignature().getDeclaringTypeName() + "." + point.getSignature().getName());
//		System.out.println("--------------afterRunningLog----------------参数为：" + Arrays.toString(point.getArgs()));
//		System.out.println("--------------afterRunningLog----------------返回值：" + returnValue);
//
//	}
//
//	@AfterThrowing(value = "allMethod()", throwing = "ex")
//	public void AfterThrowingLog(Throwable ex) {
//		System.out.println("--------------AfterThrowingLog--------------进入异常通知");
//		System.out.println("--------------AfterThrowingLog--------------异常信息：" + ex.getMessage());
//	}
//
//	@Around("allMethod()")
//	public Object doAround(ProceedingJoinPoint p) throws Throwable {
//		System.out.println("--------------Around--------------进入环绕通知");
//		long startTime = System.currentTimeMillis();
//		Object obj = p.proceed();
//		long endTime = System.currentTimeMillis();
//		MethodSignature signature = (MethodSignature) p.getSignature();
//		String methodName = signature.getDeclaringTypeName() + "." + signature.getName();
//
//		System.out.println("目标方法：" + methodName);
//		System.out.println("运行耗时：" + (endTime - startTime) + "ms");
//		System.out.println("--------------Around--------------结束方法调用");
//		return obj;
//
//	}
}
