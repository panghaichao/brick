package com.csoft.brick.common.utils.id;

import java.util.UUID;

import com.csoft.brick.common.utils.date.DateUtil;

public class IDGeneratorUtil {
	
	
	/**
	 *  以当前时间作为Id 年月日时分秒毫秒
	 * @return 20170511161434527
	 */
	public static String getCurrentDateAsId(){
		return DateUtil.getShortDateTime();
	}
	
	
	/**
	 *  以UUID作为Id 
	 * @return 62b362f4-a214-46fa-80c1-c973f0471964
	 */
	public static String getUuidAsId(){
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
	
	public static void main(String[] args) {
		System.out.println(IDGeneratorUtil.getUuidAsId());
	}
}
