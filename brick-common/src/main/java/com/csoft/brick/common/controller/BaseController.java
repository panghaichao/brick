package com.csoft.brick.common.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.csoft.brick.common.mail.MailUtil;
import com.csoft.brick.common.redis.RedisDAO;

/**
 * 
 * 基础控制层
 * 
 * @author Hejeo 2017年6月10日 22:35:51
 *
 */
public class BaseController {
	protected static Logger logger = LogManager.getLogger();

	/**
	 * 单个操作redis
	 */
	@Autowired
	protected RedisDAO redisDAO;
	
	/**
	 * 发邮件
	 */
	@Autowired
	protected MailUtil mailUtil;

	/**
	 * 获取当前登录用户对象
	 * 
	 * @return Object
	 */
	public Object getCurrentUser() {
		return SecurityUtils.getSubject().getPrincipal();
	}
}
